package workWithData;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Stepan on 08.03.16.
 */
public class PrivateBeachIntoFactTable {
    public static void main(String[] args) throws IOException {
        List<String> namesOfBeachList = Arrays.asList("пляж");

        List<String> privateWords = Arrays.asList("частный", "собственный", "закрытый", "отдельный", "платный","принадлежать","отельный","местный");
        List<String> publicWords = Arrays.asList("общественный", "городской", "общий", "бесплатно");

        List<String> negativeList = Arrays.asList("true", "нет", "без", "отсутствие",
                "лишенный", "лишивший", "мало", "никакой", "ни",
                "не", "нельзя", "невозможно", "ничего");

        String privateValueInHotel = "private_beach=true";
        String publicValueInHotel = "public_beach=true";

        StringBuilder builder = new StringBuilder();


        for (String nameOfbeach : namesOfBeachList) {
            for (String privateWord : privateWords) {
                builder.append(nameOfbeach).append("=").append(privateWord).append(" ").append(privateValueInHotel).append("\n");
            }

            for (String privateWord : privateWords) {
                for (String negative : negativeList) {
                    builder.append(nameOfbeach).append("=").append(negative).append(privateWord).append(" ").append(publicValueInHotel).append("\n");
                }
            }

            for (String publicWord : publicWords) {
                builder.append(nameOfbeach).append("=").append(publicWord).append(" ").append(publicValueInHotel).append("\n");
            }

            for (String publicWord : publicWords) {
                for (String negative : negativeList) {
                    builder.append(nameOfbeach).append("=").append(negative).append(publicWord).append(" ").append(privateValueInHotel).append("\n");
                }
            }

        }

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/privateBeach.txt"))) {
            out.print(builder.toString());
        }


    }
}
