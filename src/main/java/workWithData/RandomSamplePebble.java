package workWithData;

import metric.MetricForBeach;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

/**
 * Created by Stepan on 02.12.15.
 */
public class RandomSamplePebble {
    static int width = 5;
    static int count = 10;


    public static void main(String[] args) throws IOException {
        List<String> randomSampleName = MetricForBeach.getRandomSampleName();
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            int r = random.nextInt(randomSampleName.size());
            String hotel = randomSampleName.get(r);
            String[] p = hotel.split(" ");

            String name = p[0];
            String feature = p[1];
            System.out.println("----" + name + "----" + feature);


            BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reviews/1_top_100Sample/" + name + ".out"));
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains(feature)) {
                    StringTokenizer tokenizer = new StringTokenizer(line);
                    StringBuilder builderPhraseFeatures = new StringBuilder();
                    int index = 0;
                    while (tokenizer.hasMoreTokens()) {

                        String word = tokenizer.nextToken();
                        index++;

                        if (word.contains(feature)) {
                            //предыдущие width слов
                            StringTokenizer tokenizerSkip = new StringTokenizer(line);
                            for (int j = 0; j < index - width; j++) {
                                tokenizerSkip.nextToken();
                            }
                            for (int j = 0; j < width - 1; j++) {
                                builderPhraseFeatures.append(tokenizerSkip.nextToken()).append(" ");
                            }

                            int j = 0;
                            builderPhraseFeatures.append(word).append(" ");
                            //следующие width слов
                            while (j < width && tokenizer.hasMoreTokens()) {
                                builderPhraseFeatures.append(tokenizer.nextToken()).append(" ");
                                j++;
                            }

                        }
                    }
                    System.out.println(builderPhraseFeatures.toString());
                }
            }

            System.out.println();
        }

    }
}
