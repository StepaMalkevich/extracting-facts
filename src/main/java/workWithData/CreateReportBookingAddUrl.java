package workWithData;

import java.io.*;

/**
 * Created by Stepan on 25.04.16.
 */
public class CreateReportBookingAddUrl {

    static StringBuilder builder = new StringBuilder();

    public static void main(String[] args) throws IOException {
//        builder.append("Name").append("\t").append("Url").append("\t").append("conclusionFromReviews").append("\n");
        BufferedReader br1 = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsName.out"));
        BufferedReader br2 = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/beachBookingReport_v5.tsv"));

        String lineInbr2 = br2.readLine();
        String lineInbr1;

        while ((lineInbr2 = br2.readLine()) != null) {
            br1 = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsName.out"));
            lineInbr1 = br1.readLine();
            String[] p2 = lineInbr2.split("\t");
            String[] p1 = lineInbr1.split("\t");

            try {

                while (!p1[0].equals(p2[0])) {
                    lineInbr1 = br1.readLine();
                    p1 = lineInbr1.split("\t");
                }

                builder.append(p2[0]).append("\t").append(p1[1]).append("\t").append(p2[1]).append("\n");
//            System.out.print(p2[0] + "\t" + p1[1] + "\t" + p2[1] + "\n");
            } catch (NullPointerException ignored) {

            }

        }

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/beachBookingReportWithUrl_v5.tsv"))) {
            out.print(builder.toString());
        }

    }
}
