package workWithData;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 02.12.15.
 */
public class CreateFileTopHotels {

    public static class ValueComparator implements Comparator {
        Map<String, Integer> base;

        public ValueComparator(Map base) {
            this.base = base;
        }

        @Override
        public int compare(Object o1, Object o2) {
            if (base.get(o1) >= base.get(o2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public static List<String> getTop100IdHotels() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/top_hotels"));
        Map<String, Integer> map = new HashMap<>();
        ValueComparator valueComparator = new ValueComparator(map);
        Map<String, Integer> treeMap = new TreeMap<>(valueComparator);

        List<String> top100Hotels = new ArrayList<>();

        String line;

        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), Integer.valueOf(tokenizer.nextToken()));
        }

        treeMap.putAll(map);

        int i = 0;
        for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
            top100Hotels.add(entry.getKey());
            i++;
            if (i == 100) {
                break;
            }
        }

        return top100Hotels;
    }

    public static List<String> getSecondTop100IdHotels() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/top_hotels"));
        Map<String, Integer> map = new HashMap<>();
        ValueComparator valueComparator = new ValueComparator(map);
        Map<String, Integer> treeMap = new TreeMap<>(valueComparator);

        List<String> top200Hotels = new ArrayList<>();

        String line;

        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), Integer.valueOf(tokenizer.nextToken()));
        }

        treeMap.putAll(map);

        int i = 0;
        for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
            if (i < 100) {
                i++;
                continue;
            }
            System.out.println(entry.getKey() + " " + entry.getValue());
            top200Hotels.add(entry.getKey());
            i++;
            if (i == 200) {
                break;
            }
        }

        return top200Hotels;
    }


    public static void main(String[] args) throws IOException {
        createFileTop200();
    }

    public static void createFileTop200() throws IOException {
        //берем нужное количество id отелей в отстортированном списке по количеству отзывов
        List<String> top200Hotels = getSecondTop100IdHotels();
        System.out.println(top200Hotels.size());

        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/hotels_for_stepa.tsv"));
        String line;
        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/2_top_100Hotels.tsv"))) {

            //проходим по всему файлу с отелями для поиска выбранных отелей с выбранным id
            while ((line = br.readLine()) != null) {
                StringTokenizer tokenizer = new StringTokenizer(line, "\t");
                int id = Integer.valueOf(tokenizer.nextToken());
                if (top200Hotels.contains(String.valueOf(id))) {
                    out.println(line);
                }
            }
        }
    }


}
