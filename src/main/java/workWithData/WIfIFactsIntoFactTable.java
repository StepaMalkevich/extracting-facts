package workWithData;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Stepan on 06.03.16.
 */
public class WIfIFactsIntoFactTable {
    public static void main(String[] args) throws IOException {
        List<String> namesOfWiFiList = Arrays.asList("wi-fi",
                "сеть", "вай-фай", "беспроводной", "wi fi", "вай фай", "вай файв", "ва фай", "ви фи", "фай вай",
                "wireless", "беспроводная сеть", "ви-фи", "wan", "вафушка", "хай-фай", "вафля", "интернет", "беспроводной интернет");

        List<String> freeWords = Arrays.asList("бесплатный",",бытьбесплатен", "бесплатен", "бытьбесплатный",
                "бытьзадополнительнуюплату", "халявный", "бытьхалявный","\"бесплатный\"","каждой-бесплатный");
        List<String> paidWords = Arrays.asList("платный", "дорогой", "стоить","небытьбесплатный","бытьплатный", "бытьдорогой",
                "бытьстоить","дополнительный","заплатитьвмагазине","заплатить","якупить","заплаченный","дешевый","купитьнедельныйпропуск",
                "оплачен","бытьнаплату");

        List<String> negativeList = Arrays.asList("true","нет", "без", "отсутствие",
                "лишенный", "лишивший", "мало", "никакой", "ни",
                "не", "нельзя", "невозможно", "ничего");

        String freeValueInHotel = "internet_in_hotel=free_wi_fi_in_public_areas";
        String paidValueInHotel = "internet_in_hotel=paid_wi_fi_in_public_areas";

        StringBuilder builder = new StringBuilder();


        for (String nameOfWiFi : namesOfWiFiList) {
            for (String freeWord : freeWords) {
                builder.append(nameOfWiFi).append("=").append(freeWord).append(" ").append(freeValueInHotel).append("\n");
            }

            for (String freeWord : freeWords) {
                for (String negative : negativeList) {
                    builder.append(nameOfWiFi).append("=").append(negative).append(freeWord).append(" ").append(paidValueInHotel).append("\n");
                }
            }

            for (String paidWord : paidWords) {
                builder.append(nameOfWiFi).append("=").append(paidWord).append(" ").append(paidValueInHotel).append("\n");
            }
        }

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/wi-fiFree.txt"))) {
            out.print(builder.toString());
        }


    }

}
