package statistic;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Stepan on 17.02.16.
 */
public interface Statistic {
    Map<String, Integer> getStatistic(String sampleNumber, String word, String hotelsSampleFile) throws IOException;
}
