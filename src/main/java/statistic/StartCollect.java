package statistic;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Created by Stepan on 25.11.15.
 */
public class StartCollect {
    public static void main(String[] args) throws IOException {
        int param = 2;
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/2_top_100Hotels.tsv";

        int sampleNumber = 2;

        //доделать с именами файлов, тоже их удобно передавать
        StatisticImpl statistic = new StatisticImpl();

        switch (param) {
            case 0:
                getSomeWork(String.valueOf(sampleNumber), hotelsSampleFile, statistic, StartCollect::collectionOfWordsBeach);
                break;
            case 1:
                getSomeWork(String.valueOf(sampleNumber), hotelsSampleFile, statistic, StartCollect::collectionOfWordsWiFi);
                break;
            case 2:
                getSomeWork(String.valueOf(sampleNumber), hotelsSampleFile, statistic, StartCollect::collectionOfEnterToSea);
                break;
            case 3:
                getSomeWork(String.valueOf(sampleNumber), hotelsSampleFile, statistic, StartCollect::collectionOfTransfer);
                break;
            default:
                throw new IllegalStateException();

        }


    }

    private static void getSomeWork(String sampleNumber, String hotelsSampleFile, Statistic statistic, Supplier<List<String>> collectionOfParam) throws IOException {
        for (String word : collectionOfParam.get()) {

            System.out.println("keyWord = " + word);

            Map<String, Integer> map = statistic.getStatistic(sampleNumber, word, hotelsSampleFile);
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                if (entry.getValue() > 0) {
                    System.out.println(entry.getKey() + "=" + entry.getValue());
                }
            }
        }
    }

    //добавить слова в словарь
    public static List<String> collectionOfWordsWiFi() {
        return Arrays.asList("wi-fi", "сеть", "вай-фай", "беспроводной", "wi fi", "вай фай", "вай файв", "ва фай", "ви фи", "фай вай",
                "wireless", "беспроводная сеть", "ви-фи", "wan", "вафушка", "хай-фай", "вафля", "интернет", "беспроводной интернет");
    }

    public static List<String> collectionOfWordsBeach() {
        return Arrays.asList("пляж", "песок", "галька", "заход", "дно", "кораллы", "вход");
    }

    public static List<String> collectionOfEnterToSea() {
        return Arrays.asList("вход в море", "входвморе", "вход", "море", "вморе", "заход", "дно", "спуск");
    }

    public static List<String> collectionOfTransfer() {
        return Arrays.asList("трансфер");
    }


}
