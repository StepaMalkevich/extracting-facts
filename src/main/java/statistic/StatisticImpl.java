package statistic;

import exstractFacts.Facts;
import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 08.03.16.
 */
public class StatisticImpl implements Statistic {
    public class ValueComparator implements Comparator {
        Map<String, Integer> base;

        public ValueComparator(Map base) {
            this.base = base;
        }

        @Override
        public int compare(Object o1, Object o2) {
            if (base.get(o1) >= base.get(o2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public Map<String, Integer> getStatistic(String sampleNumber, String keyWord, String hotelsSampleFile) throws IOException {

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        Map<String, Integer> map = new HashMap<>();
        ValueComparator valueComparator = new ValueComparator(map);

        Map<String, Integer> treeMap = new TreeMap<>(valueComparator);

        for (Hotel h : hotels) {

            try {

                String name = h.getName().replaceAll(" ", "");

                Map<String, Map<String, Integer>> facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);

                for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                            map.compute(in.getKey(), (w, f) -> f == null ? 1 : f + in.getValue());
                        }
                    }
                }
            } catch (FileNotFoundException ignored) {

            }

        }

        treeMap.putAll(map);
        return treeMap;

    }
}
