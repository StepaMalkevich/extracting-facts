package metricBooking;

import exstractFacts.Facts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 15.04.16.
 */
public class MetricForTransferBooking {

    public static void main(String[] args) throws IOException {
        getMetricsRes();
    }

    public static void getMetricsRes() throws IOException {
        int number = 1;
        int bookingNumber;
        String fileWithHotelsName;

        switch (number) {
            case 1:
                bookingNumber = -1;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotelsMyGrammar.out";
                break;
            default:
                throw new IllegalStateException();

        }


        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;
        int noInfo = 0;
        int exp = 0;
        int fileCount = 0;

        Map<String, Integer> beachMap = new HashMap<>();
        Map<String, Integer> aeroMap = new HashMap<>();
        Map<String, Integer> hotelMap = new HashMap<>();


        BufferedReader br = new BufferedReader(new FileReader(fileWithHotelsName));
        String line;

        List<String> hotels = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, "\t");
            hotels.add(tokenizer.nextToken());
            fileCount++;
        }


        for (String name : hotels) {
            //false url!!!

            String info = name + "\n";
            double privateBeach = 0;
            double publicBeach = 0;


            //почему-то некоторых отелей нет
            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOutBooking(bookingNumber, "Tomita" + name);
            } catch (NoSuchElementException | IOException e) {
                noReviews++; // нет отелей => нет отзывов
                exp++;
                continue;
            }

            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                for (String keyWord : Arrays.asList("трансфер")) {

                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
//                            System.out.println(entry.getKey() + "=" + in.getKey());
                            if (in.getKey().contains("пляж")) {
                                beachMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                            if (in.getKey().contains("аэро")) {
                                aeroMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                            if (in.getKey().contains("отел")) {
                                hotelMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                        }

                    }

                }

            }
        }

        System.out.println("exception count = " + exp);

        System.out.println("трансфер до пляжа\nколичество отелей из " + fileCount + " = "
                + beachMap.size() + "\nколичество отелей, в которых на самом деле пишут про трансфер до пляжа = ?");
        System.out.println("трансфер до аэропорта\nколичество отелей из " + fileCount + " = "
                + aeroMap.size() + "\nколичество отелей, в которых на самом деле пишут про трансфер до аэропорта = ?");
        System.out.println("трансфер до отеля\nколичество отелей из " + fileCount + " = "
                 + hotelMap.size() + "\nколичество отелей, в которых на самом деле пишут про трансфер до отеля = ?");


    }

}
