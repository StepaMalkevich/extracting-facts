package metricBooking.metricCount;

import java.io.*;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Stepan on 09.05.16.
 */
public class ARPCount {

    static StringBuilder FpFnUrls = new StringBuilder();
    static int ulrCNt = 0;
    static int n = 1145;
    static int[] randomArr = new int[n];

    static double trueNegative = 0;
    static double truePositive = 0;
    static double falseNegative = 0;
    static double falsePositive = 0;
//    static String fact = "sand";

    static String factInAlg = "mixed"; //paid
    static String factInBase = "mixed"; //paid_wi_fi
    static Random random = new Random();
    static Set<Integer> set = new HashSet<>();

    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 100; i++) {
            int r = random.nextInt(n);
            while (set.contains(r)) {
                r = random.nextInt(n);
            }
            set.add(r);
            randomArr[r] = 1;
        }

        FpFnUrls.append("Result\t").append("url\t").append("1/0").append("\n");
        getCount();
    }

    public static void getCount() throws IOException {

        //New Hotel	http://www.booking.com/hotel/ge/new.html	mixed
        //http://www.booking.com/hotel/tn/residence-diar-lemdina.html	beach	sand

        int exc_cnt = 0;
        BufferedReader brBase;
        BufferedReader brAlg = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/beachBookingReportWithUrl_v4.tsv"));
        //9_000
        String line;

        int cc = 0;

        while ((line = brAlg.readLine()) != null) {
            String[] p = line.split("\t");

            cc++;
            String url = p[1];
            String valueInAlg = p[2];

            brBase = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking_features_only_one.tsv"));
            //11_000
            String lineInbrBase = brBase.readLine();
            String[] p2 = lineInbrBase.split("\t");

            try {

                while (!p2[0].equals(url)) {
                    lineInbrBase = brBase.readLine();
                    p2 = lineInbrBase.split("\t");
                }

                //internet_in_hotel
                if (p2[1].equals("beach")) {
                    String valueInBase = p2[2];

                    String innerUrl = p2[0];

                    getCountAllMetric(innerUrl, valueInAlg, valueInBase);

                }

            } catch (NullPointerException e) {
                exc_cnt++;
            }

        }


        System.out.println("TP " + truePositive);
        System.out.println("TN " + trueNegative);
        System.out.println("FP " + falsePositive);
        System.out.println("FN " + falseNegative);

        getStatistic(truePositive, trueNegative, falsePositive, falseNegative);
//        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/Desktop/FP&FN/free.tsv"))) {
//            out.println(FpFnUrls.toString());
//        }


//        beach =sand
//        TP 267.0
//        TN 459.0
//        FP 74.0
//        FN 1071.0
//        accuracy = 0.3880277926242651
//        precision = 0.782991202346041
//        recall = 0.19955156950672645
//
//        beach = pebble
//        TP 166.0
//        TN 1352.0
//        FP 187.0
//        FN 166.0
//        accuracy = 0.8113308391234634
//        precision = 0.4702549575070821
//        recall = 0.5

//        beach = mixed
//        TP 111.0
//        TN 601.0
//        FP 1066.0
//        FN 93.0
//        accuracy = 0.3805451630144308
//        precision = 0.09430756159728122
//        recall = 0.5441176470588235

//        wi-fi = free
//        TP 5609.0
//        TN 11.0
//        FP 236.0
//        FN 155.0
//        accuracy = 0.9349525869239728
//        precision = 0.9596236099230111
//        recall = 0.9731089521165857

//        wi-fi = paid
//        TP 10.0
//        TN 5631.0
//        FP 224.0
//        FN 146.0
//        accuracy = 0.9384461819996672
//        precision = 0.042735042735042736
//        recall = 0.0641025641025641


    }

    private static void getCountAllMetric(String innerUrl, String valueInAlg, String valueInBase) {

        if (valueInAlg.contains(factInAlg) && valueInBase.contains(factInBase)) {
            truePositive++;
        } else {
            if (valueInAlg.contains(factInAlg) && !valueInBase.contains(factInBase)) {
                falsePositive++;
                if (randomArr[ulrCNt] == 1) {
                    FpFnUrls.append("FP\t").append(innerUrl).append("\t ").append("\n");
                }
                ulrCNt++;
            } else {
                if (!valueInAlg.contains(factInAlg) && valueInBase.contains(factInBase)) {
                    falseNegative++;
                    if (randomArr[ulrCNt] == 1) {
                        FpFnUrls.append("FN\t").append(innerUrl).append("\t ").append("\n");
                    }
                    ulrCNt++;
                } else {
                    if (!valueInAlg.contains(factInAlg) && !valueInBase.contains(factInBase)) {
                        trueNegative++;
                    }
                }
            }
        }
    }

    private static void getStatistic(double truePositive, double trueNegative, double falsePositive, double falseNegative) {
        double ACC = (truePositive + trueNegative) / (truePositive + trueNegative + falsePositive + falseNegative);

        double precision = truePositive / (truePositive + falsePositive);

        double recall = truePositive / (truePositive + falseNegative);


        System.out.println("accuracy = " + ACC);
        System.out.println("precision = " + precision);
        System.out.println("recall = " + recall);
    }

}

