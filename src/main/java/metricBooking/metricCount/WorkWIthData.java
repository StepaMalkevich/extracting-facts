package metricBooking.metricCount;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 09.05.16.
 */
public class WorkWIthData {

    //    http://www.booking.com/hotel/ru/apartment-breeze-adler1.html
//    http://www.booking.com/hotel/in/oasis-mumbai.html
//    http://www.booking.com/hotel/ru/guesthouse-radost-39-anapa.html
//    http://www.booking.com/hotel/xa/guest-house-u-anzhely.html
//    http://www.booking.com/hotel/ru/feya-2-anapa1.html
    public static void main(String[] args) throws IOException {
        BufferedReader brAlg = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking_features.tsv"));
        String line;
        Map<String, Integer> map = new HashMap<>();

        int cc = 0;
        while ((line = brAlg.readLine()) != null) {
            String[] p = line.split("\t");
            if (p[1].equals("internet_in_hotel")) {
                map.compute(p[0], (k, v) -> v == null ? 1 : v + 1);
                cc++;
            }
        }

        Set<String> set = new HashSet<>();
        final int[] wtf = {0, 0, 0, 0, 0};
        map.forEach((k, v) -> {
            if (v == 0) {
                wtf[0]++;
            }
            if (v == 1) {
                wtf[1]++;
                set.add(k);
            }
            if (v == 2) {
                wtf[2]++;
            }
            if (v == 3) {
                wtf[3]++;
            }
            if (v >= 4) {
                wtf[4]++;
            }
        });

        System.out.println(wtf[0]);
        System.out.println(wtf[1]);
        System.out.println(wtf[2]);
        System.out.println(wtf[3]);
        System.out.println(wtf[4]);

        brAlg = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking_features.tsv"));
        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/wi-fi_booking_features_only_one.tsv"))) {

            while ((line = brAlg.readLine()) != null) {
                String[] p = line.split("\t");
                if (set.contains(p[0]) && p[1].equals("internet_in_hotel")) {
                    out.println(line);
                }
            }
        }

    }

    public static Set<String> getHotelsNameWithBeach() throws IOException {
        BufferedReader brAlg = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking_features.tsv"));
        String line;
        Set<String> strings = new HashSet<>();
        while ((line = brAlg.readLine()) != null) {
            String[] p = line.split("\t");
            if (p[1].equals("beach")) {
                strings.add(p[0]);
            }
        }
        return strings;
    }
}
