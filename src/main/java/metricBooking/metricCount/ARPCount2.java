package metricBooking.metricCount;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Stepan on 09.05.16.
 */
public class ARPCount2 {
//проблема с количеством пересечения
    static double trueNegative = 0;
    static double truePositive = 0;
    static double falseNegative = 0;
    static double falsePositive = 0;
//    static String fact = "pebble";
    static String factInAlg = "mixed";
    static String factInBase = "mixed";

    public static void main(String[] args) throws IOException {
        getCount();
    }

    public static void getCount() throws IOException {

        //New Hotel	http://www.booking.com/hotel/ge/new.html	mixed
        //http://www.booking.com/hotel/tn/residence-diar-lemdina.html	beach	sand

        int excCnt = 0;
        BufferedReader brBase;

        brBase = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking_features_only_one.tsv"));
        String line;

        while ((line = brBase.readLine()) != null) {
            String[] p1 = line.split("\t");

            String url = p1[0];
            //p[1] = beach, такой файл создал
            String valueInBase = p1[2];

            BufferedReader brAlg = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/beachBookingReportWithUrl_v2.tsv"));

            String lineInbrAlg = brAlg.readLine();
            String[] p2 = lineInbrAlg.split("\t");

            try {

                while (!p2[1].equals(url)) {
                    lineInbrAlg = brAlg.readLine();
                    p2 = lineInbrAlg.split("\t");
                }

                String valueInAlg = p2[2];
                getCountAllMetric(valueInAlg, valueInBase);
            } catch (NullPointerException e) {
                excCnt++;
            }

        }


        System.out.println("TP " + truePositive);
        System.out.println("TN " + trueNegative);
        System.out.println("FP " + falsePositive);
        System.out.println("FN " + falseNegative);
        System.out.println("not find url " + excCnt);

        getStatistic(truePositive, trueNegative, falsePositive, falseNegative);
        // beach = sand
//        TP 543.0
//        TN 88.0
//        FP 181.0
//        FN 85.0
//        not find url 10876
//        accuracy = 0.7034559643255296
//        precision = 0.75
//        recall = 0.8646496815286624

//beach = pebble
//        TP 142.0
//        TN 156.0
//        FP 585.0
//        FN 14.0
//        not find url 10876
//        accuracy = 0.33221850613154963
//        precision = 0.1953232462173315
//        recall = 0.9102564102564102

        //всего у меня 9000 записей про отлели с beach, так как столько имеют что-то про пляж в отзывах
        //в файле base там 11700 записей, где можно однозначно понять какой тип пляжа

        //beach = mixed
//        TP 52.0
//        TN 296.0
//        FP 502.0
//        FN 47.0
//        not find url 10876
//        accuracy = 0.3879598662207358
//        precision = 0.09386281588447654
//        recall = 0.5252525252525253

// wi-fi = free
//        TP 1945.0
//        TN 7.0
//        FP 66.0
//        FN 33.0
//        not find url 157480
//        accuracy = 0.9517308629936616
//        precision = 0.9671805072103431
//        recall = 0.9833164812942367
// wi-fi = paid
//        TP 5.0
//        TN 1970.0
//        FP 53.0
//        FN 23.0
//        not find url 157480
//        accuracy = 0.9629449049244271
//        precision = 0.08620689655172414
//        recall = 0.17857142857142858

    }

    private static void getCountAllMetric(String valueInAlg, String valueInBase) {

        if (valueInAlg.contains(factInAlg) && valueInBase.contains(factInBase)) {
            truePositive++;
        } else {
            if (valueInAlg.contains(factInAlg) && !valueInBase.contains(factInBase)) {
                falsePositive++;
            } else {
                if (!valueInAlg.contains(factInAlg) && valueInBase.contains(factInBase)) {
                    falseNegative++;
                } else {
                    if (!valueInAlg.contains(factInAlg) && !valueInBase.contains(factInBase)) {
                        trueNegative++;
                    } else {
                        System.out.println("it can not be 2");
                    }
                }
            }
        }
    }

    private static void getStatistic(double truePositive, double trueNegative, double falsePositive, double falseNegative) {
        double ACC = (truePositive + trueNegative) / (truePositive + trueNegative + falsePositive + falseNegative);

        double precision = truePositive / (truePositive + falsePositive);

        double recall = truePositive / (truePositive + falseNegative);


        System.out.println("accuracy = " + ACC);
        System.out.println("precision = " + precision);
        System.out.println("recall = " + recall);
    }

}

