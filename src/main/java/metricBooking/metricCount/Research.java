package metricBooking.metricCount;

import java.io.*;

/**
 * Created by Stepan on 11.05.16.
 */
public class Research {
    public static void main(String[] args) throws IOException {
        BufferedReader brAlg = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/beachBookingReportWithUrl.tsv"));
        BufferedReader brBase = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/wi-fi_booking_features_only_one.tsv"));
        StringBuilder builder = new StringBuilder();

        String line;
        int ccIntersect = 0;
        int ccInAlg = 0;
        int ccInBase = 0;
        boolean flag = true;
        while ((line = brBase.readLine()) != null) {
            String[] p = line.split("\t");
            String url = p[0];
            brAlg = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/wi-fiBookingReportWithUrl.tsv"));
            String lineInALg;
            while ((lineInALg = brAlg.readLine()) != null) {
                String[] p1 = lineInALg.split("\t");
                if (p1[1].equals(url)) {
                    ccIntersect++;
                    builder.append(p1[0]).append("\n");
                }


                if (flag) {
//                    ccInBase++;
                    ccInAlg++;
                }
            }
            flag = false;
//            ccInAlg++;
            ccInBase++;
        }


        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/hotelsOnWi-fi.tsv"))) {
            out.print(builder.toString());
        }

//        System.out.println("alg = " + ccInAlg);
//        System.out.println("base = " + ccInBase);
//        System.out.println("intersect = " + ccIntersect);
    }


//for alg for base
//    alg = 9928
//    base = 11773
//    intersect = 1871


    //for base for alg
//    alg = 9928
//    base = 11773
//    intersect = 1871

}
