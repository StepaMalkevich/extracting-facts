package metricBooking;

import exstractFacts.Facts;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 06.04.16.
 */
public class MetricForWiFiBooking {

    static StringBuilder builder = new StringBuilder();

    public static void main(String[] args) throws IOException {
        getMetricsRes();
    }

    public static void getMetricsRes() throws IOException {
        int number = 5;
        int bookingNumber;
        String fileWithHotelsName;

        switch (number) {
            case 2:
                bookingNumber = 2;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels2.out";
                break;
            case 3:
                bookingNumber = 3;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsNameUniqueLine.out";
                break;
            case 4:
                bookingNumber = 4;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels4.out";
                break;
            case 5:
                bookingNumber = 5;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels5.out";
                break;
            default:
                throw new IllegalStateException();

        }


        Map<String, String> table = getTable();


        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;
        int infoCnt = 0;
        int exp = 0;
        int fileCount = 0;

        BufferedReader br = new BufferedReader(new FileReader(fileWithHotelsName));
        String line;

        List<String> hotels = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, "\t");
            hotels.add(tokenizer.nextToken());
            fileCount++;
        }

//        builder.append("Name").append("\t").append("conclusionFromReviews").append("\n");

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/wi-fiBookingReport.tsv", true))) {

            for (String name : hotels) {
                //false url!!!

                String info = name + "\n";
                double free = 0;
                double paid = 0;


                //почему-то некоторых отелей нет
                Map<String, Map<String, Integer>> facts;
                try {
                    facts = Facts.getFactsFromTomitaOutBooking(bookingNumber, "Tomita" + name);
                } catch (NoSuchElementException | IOException e) {
                    noReviews++; // нет отелей => нет отзывов
                    exp++;
                    continue;
                }

                double allFacts = facts.size();
                info = info + "allFacts = " + allFacts + "\n";


                for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                    for (String keyWord : Arrays.asList("wi-fi", "сеть", "вай-фай", "беспроводной", "wi fi", "вай фай", "вай файв", "ва фай", "ви фи", "фай вай",
                            "wireless", "беспроводная сеть", "ви-фи", "wan", "вафушка", "хай-фай", "вафля", "интернет", "беспроводной интернет")) {

                        if (entry.getKey().equals(keyWord)) {
                            for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                                String s = keyWord + "=" + in.getKey();
                                if (table.containsKey(s)) {
                                    info = info + "reviews : " + s + "     " + table.get(s) + " count = " + in.getValue() + "\n";

                                    if (table.get(s).equals("internet_in_hotel=free_wi_fi_in_public_areas")) {
                                        free += in.getValue();
                                    } else {
                                        if (table.get(s).equals("internet_in_hotel=paid_wi_fi_in_public_areas")) {
                                            paid += in.getValue();
                                        }
                                    }
                                }


                            }
                        }

                    }

                }


                info = info + free + " " + paid + "\n";

                String conclusionFromReviews = "";

                if (free == paid && free == 0) {
                    conclusionFromReviews = "nothing about wi-fi";
                } else {
                    conclusionFromReviews = getConclusion(1 * free, 1 * paid);
                    infoCnt++;
                    info = info + "conclusion = " + conclusionFromReviews + "\n";
                    builder.append(name).append("\t").append(conclusionFromReviews).append("\n");
                }


            }

            System.out.println("infoCnt about wi-fi = " + infoCnt + " hotels from " + fileCount + " hotels");
            System.out.println("exception count = " + exp);

            out.print(builder.toString());
        }
    }

    private static String getConclusion(double countFree, double countPaid) {
        String conclusion;

        //этот if означает, что если так, то я соглашаюсь с базой и неважно, что там free или paid
        if (Math.abs(countFree - countPaid) < 1) {
            conclusion = "free paid";
            return conclusion;
        }
        if (countFree > countPaid) {
            return "free";
        } else {
            return "paid";
        }
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/wi-fiFree.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }


        return map;
    }
}
