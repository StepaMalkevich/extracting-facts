package metricBooking;

import exstractFacts.Facts;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 14.11.15.
 */
public class MetricForBeachBooking {

    static Map<String, List<String>> columnBaseAlgorithm = new HashMap<>();
    static List<String> randomSampleName = new ArrayList<>();
    static StringBuilder builder = new StringBuilder();

    public static void main(String[] args) throws IOException {
        getMetricsRes();
    }

    public static void getMetricsRes() throws IOException {
        int number = 5;
        int bookingNumber;
        String fileWithHotelsName;

        switch (number) {
            case 2:
                bookingNumber = 2;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels2.out";
                break;
            case 3:
                bookingNumber = 3;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsNameUniqueLine.out";
                break;
            case 4:
                bookingNumber = 4;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels4.out";
                break;
            case 5:
                bookingNumber = 5;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels5.out";
                break;
            default:
                throw new IllegalStateException();
        }


        Map<String, String> table = getTable();
        List<String> listKeyWords = Arrays.asList("пляж", "песок", "заход", "дно");

        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;
        int InfoCnt = 0;
        int exp = 0;
        int fileCount = 0;

        BufferedReader br = new BufferedReader(new FileReader(fileWithHotelsName));
        String line;

        List<String> hotels = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, "\t");
            hotels.add(tokenizer.nextToken());
            fileCount++;
        }

//        builder.append("Name").append("\t").append("conclusionFromReviews").append("\n");
        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/beachBookingReport_v5.tsv", true))) {


            for (String name : hotels) {
                //false url!!!

                String info = name + "\n";
                double countSand = 0;
                double countPebble = 0;
                double countMixed = 0;


                //почему-то некоторых отелей нет
                Map<String, Map<String, Integer>> facts;
                try {
                    facts = Facts.getFactsFromTomitaOutBooking(bookingNumber, "Tomita" + name);
                } catch (NoSuchElementException | IOException e) {
                    noReviews++; // нет отелей => нет отзывов
                    exp++;
                    continue;
                }

                double allFacts = facts.size();
                info = info + "allFacts = " + allFacts + "\n";


                for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {


                    for (String keyWord : listKeyWords) {
                        if (entry.getKey().equals(keyWord)) {
                            for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                                // тут нужно достать in.getKey() in.getValue() <-тогда слово встречается в отзывах не 1 раз, а in.getValue()
                                String s = keyWord + "=" + in.getKey();
                                if (table.containsKey(s)) {
                                    //нужно пойти и посмотерть чему равно значение в базе и сравнить его с table.get(s)
                                    //руководствуемся мнением большинства
                                    info = info + "reviews : " + table.get(s) + " count = " + in.getValue() + "\n";

                                    if (table.get(s).equals("beach=sand")) {
                                        countSand += in.getValue();
                                    } else {
                                        if (table.get(s).equals("beach=pebble")) {
                                            countPebble += in.getValue();
                                        } else {
                                            if (table.get(s).equals("beach=mixed_sand_pebble")) {
                                                countMixed += in.getValue();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    //добавил новые слова, описывающие факт пляж=песчаный
                    if (entry.getKey().equals("галька")) {
                        int cc = entry.getValue().size();
                        countPebble += cc;
                        info = info + "reviews : beach=pebble count = " + cc + "\n";
                    }


                }


                info = info + countSand + " " + countPebble + " " + countMixed + "\n";

                String conclusionFromReviews = "";

                if (countSand == countPebble && countSand == countMixed && countSand == 0) {
                    conclusionFromReviews = "nothing about beach";
                } else {
                    conclusionFromReviews = getConclusion(1 * countSand, 2 * countPebble, 3 * countMixed);
                    info = info + "conclusion = " + conclusionFromReviews + "\n";
                    InfoCnt++;
                    builder.append(name).append("\t").append(conclusionFromReviews).append("\n");
                }


            }

            System.out.println("InfoCnt about beach = " + InfoCnt + " hotels from " + fileCount + " hotels");
            System.out.println("exception count = " + exp);


            out.print(builder.toString());
        }

    }

    public static Map<String, List<String>> getColumnList() throws IOException {
        getMetricsRes();
        return columnBaseAlgorithm;
    }

    public static List<String> getRandomSampleName() throws IOException {
        getMetricsRes();
        return randomSampleName;
    }


    private static String getConclusion(double countSand, double countPebble, double countMixed) {
        String conclusion;
        if (countSand == countPebble && countSand == countMixed) {
            conclusion = "sand pebble mixed";
            return conclusion;
        }


        //new_result
        //int lim = 6;

        //new_result_2
        //int lim = 10;

        int lim = 4;

        if (countSand + countPebble + countMixed > lim) {
            if (Math.abs(countSand - countPebble) <= 1) {
                conclusion = "mixed";
                return conclusion;
            }
        }



        if (countSand == countMixed && countPebble > countSand) {
            conclusion = "pebble";
            return conclusion;
        }
        if (countSand == countMixed && countPebble < countSand) {
            conclusion = "sand mixed";
            return conclusion;
        }

        if (countMixed == countPebble && countSand > countMixed) {
            conclusion = "sand";
            return conclusion;
        }
        if (countMixed == countPebble && countSand < countMixed) {
            conclusion = "mixed pebble";
            return conclusion;
        }
        double max = Math.max(countSand, Math.max(countMixed, countPebble));
        if (max == countSand) {
            return "sand";
        }
        if (max == countPebble) {
            return "pebble";
        }
        if (max == countMixed) {
            return "mixed";
        }
        throw new IllegalStateException();
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/beach.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }

        return map;
    }

}
