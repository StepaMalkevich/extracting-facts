package metricBooking;

import exstractFacts.Facts;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 06.04.16.
 */
public class MetricForPrivateBeachBooking {

    static StringBuilder builder = new StringBuilder();

    public static void main(String[] args) throws IOException {
        getMetricsRes();
    }

    public static void getMetricsRes() throws IOException {
        int number = 5;
        int bookingNumber;
        String fileWithHotelsName;

        switch (number) {
            case 2:
                bookingNumber = 2;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels2.out";
                break;
            case 3:
                bookingNumber = 3;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsNameUniqueLine.out";
                break;
            case 4:
                bookingNumber = 4;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels4.out";
                break;
            case 5:
                bookingNumber = 5;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels5.out";
                break;
            default:
                throw new IllegalStateException();

        }


        Map<String, String> table = getTable();

        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;
        int InfoCnt = 0;
        int exp = 0;
        int fileCount = 0;

        BufferedReader br = new BufferedReader(new FileReader(fileWithHotelsName));
        String line;

        List<String> hotels = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, "\t");
            hotels.add(tokenizer.nextToken());
            fileCount++;
        }

//        builder.append("Name").append("\t").append("conclusionFromReviews").append("\n");

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/privateBeachBookingReport.tsv", true))) {


            for (String name : hotels) {
                //false url!!!

                String info = name + "\n";
                double privateBeach = 0;
                double publicBeach = 0;


                //почему-то некоторых отелей нет
                Map<String, Map<String, Integer>> facts;
                try {
                    facts = Facts.getFactsFromTomitaOutBooking(bookingNumber, "Tomita" + name);
                } catch (NoSuchElementException | IOException e) {
                    noReviews++; // нет отелей => нет отзывов
                    exp++;
                    continue;
                }

                double allFacts = facts.size();
                info = info + "allFacts = " + allFacts + "\n";


                for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                    for (String keyWord : Arrays.asList("пляж")) {

                        if (entry.getKey().equals(keyWord)) {
                            for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                                String s = keyWord + "=" + in.getKey();
                                if (table.containsKey(s)) {
                                    info = info + "reviews : " + s + "     " + table.get(s) + " count = " + in.getValue() + "\n";

                                    if (table.get(s).equals("private_beach=true")) {
                                        privateBeach += in.getValue();
                                    } else {
                                        if (table.get(s).equals("public_beach=true")) {
                                            publicBeach += in.getValue();
                                        }
                                    }
                                }


                            }
                        }

                    }

                }


                info = info + privateBeach + " " + publicBeach + "\n";

                String conclusionFromReviews = "";

                if (privateBeach == publicBeach && privateBeach == 0) {
                    conclusionFromReviews = "nothing about wi-fi";
                } else {
                    conclusionFromReviews = getConclusion(1 * privateBeach, 1 * publicBeach);
                    info = info + "conclusion = " + conclusionFromReviews + "\n";
                    InfoCnt++;
                    builder.append(name).append("\t").append(conclusionFromReviews).append("\n");
                }


            }

            System.out.println("InfoCnt about private beach = " + InfoCnt + " from " + fileCount + " hotels");
            System.out.println("exception count = " + exp);

            out.print(builder.toString());
        }
    }

    private static String getConclusion(double countPrivate, double countPublic) {
        String conclusion;

        //этот if означает, что если так, то я соглашаюсь с базой и неважно, что там free или paid
        if (Math.abs(countPrivate - countPublic) == 0) {
            conclusion = "private public";
            return conclusion;
        }
        if (countPrivate > countPublic) {
            return "private";
        } else {
            return "public";
        }
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/privateBeach.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }


        return map;
    }
}
