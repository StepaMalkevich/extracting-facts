package metricBooking;

import exstractFacts.Facts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 15.04.16.
 */
public class MetricForEnterToSeaBooking {

    public static void main(String[] args) throws IOException {
        getMetricsRes();
    }

    public static void getMetricsRes() throws IOException {
        int number = 5;
        int bookingNumber;
        String fileWithHotelsName;

        switch (number) {
            case 2:
                bookingNumber = 2;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels2.out";
                break;
            case 3:
                bookingNumber = 3;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsNameUniqueLine.out";
                break;
            case 4:
                bookingNumber = 4;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels4.out";
                break;
            case 5:
                bookingNumber = 5;
                fileWithHotelsName = "/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels5.out";
                break;
            default:
                throw new IllegalStateException();

        }


        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;
        int noInfo = 0;
        int exp = 0;
        int fileCount = 0;

        Map<String, Integer> gentleMap = new HashMap<>();
        Map<String, Integer> uncomfortableMap = new HashMap<>();

        List<String> gentleWords = Arrays.asList("пологий", "плавный", "постепенный", "плоский");
        List<String> uncomfortableWords = Arrays.asList("неудобный", "каменистый", "крутой", "резкий", "крутоват", "глубоко", "становитьсяглубоко");


        BufferedReader br = new BufferedReader(new FileReader(fileWithHotelsName));
        String line;

        List<String> hotels = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, "\t");
            hotels.add(tokenizer.nextToken());
            fileCount++;
        }


        for (String name : hotels) {
            //false url!!!

            String info = name + "\n";
            double privateBeach = 0;
            double publicBeach = 0;


            //почему-то некоторых отелей нет
            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOutBooking(bookingNumber, "Tomita" + name);
            } catch (NoSuchElementException | IOException e) {
                noReviews++; // нет отелей => нет отзывов
                exp++;
                continue;
            }

            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                for (String keyWord : Arrays.asList("вход", "заход")) {

                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
//                            System.out.println(entry.getKey() + "=" + in.getKey());
                            if (gentleWords.contains(in.getKey())) {
                                gentleMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                            if (uncomfortableWords.contains(in.getKey())) {
                                uncomfortableMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                        }

                    }

                }

            }
        }

        System.out.println("exception count = " + exp);

        System.out.println("вход пологий\nколичество отелей из " + fileCount + " = "
                + gentleMap.size() + "\nколичество отелей, в которых на самом деле пишут про пологий вход = ?");
        System.out.println("вход крутой\nколичество отелей из " + fileCount + " = "
                + uncomfortableMap.size() + "\nколичество отелей, в которых на самом деле пишут про крутой вход= ?");


    }
}
