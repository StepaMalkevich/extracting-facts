package Tomita;

import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.*;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Stepan on 09.11.15.
 */
public class StartPassFileThroughTomita {

    public static void main(String[] args) throws IOException, InterruptedException {
        startBooking();
//        getUniqueHotelInFile();
    }


    public static void startTripAdvisor() throws IOException, InterruptedException {
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/2_top_100Hotels.tsv";
        String tomitaDirectoryIn = "\"/Users/Stepan/malkevich.stepan/extracting-facts/reviews/2_top_100Sample/";
        String tomitaDirectoryOut = "\"/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOut2_top_100Sample(MyGrammarTransfer)/";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        for (Hotel h : hotels) {
            String name = h.getName();
            String fileName = name.replaceAll(" ", "");

            System.out.println(name);

            tomitaMacExecute(tomitaDirectoryIn, tomitaDirectoryOut, fileName + ".out");
        }
    }

    public static void getUniqueHotelInFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsName.out"));
        String line;

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotels3.out"))) {

            int cnt = 0;

            for (int i = 0; i < 73_000; i++) {
                String s = br.readLine();
                System.out.println(s);
            }

            String s = "";
            while ((line = br.readLine()) != null) {
                if (cnt == 46_703) {
                    break;
                }
                s = br.readLine();
                while (line.equals(s)) {
                    s = br.readLine();
                }
                out.println(line);
                cnt++;
            }

            if (s != null) {
                out.println(s);
            }
        }

    }

    public static void startBooking() throws IOException, InterruptedException {
        String tomitaDirectoryIn = "\"/Users/Stepan/malkevich.stepan/extracting-facts/reviews/bookingHotels/";
        String tomitaDirectoryOut = "\"/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOutBookingMyGrammar/";

        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsNameUniqueLine.out"));
        String line;
        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelName/hotelsMyGrammar.out"))) {

            //всего строк 216168

//            for (int i = 0; i < (73_000 + 47_000 + 65_000); i++) {
//                br.readLine();
//            }

            while ((line = br.readLine()) != null) {
                StringTokenizer tokenizer = new StringTokenizer(line, "\t");
                String name = tokenizer.nextToken();
                out.println(name);
                tomitaMacExecute(tomitaDirectoryIn, tomitaDirectoryOut, name + ".out");
            }
        }
    }

    public static void tomitaMacExecute(String tomitaDirectoryIn, String tomitaDirectoryOut, String hotelName) throws IOException, InterruptedException {
        changeTomitaBizProtoSettings(tomitaDirectoryIn, tomitaDirectoryOut, hotelName);
        tomitaRun();
    }

    public static void tomitaRun() throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder("/Users/Stepan/Tomita/Tomita-mac", "/Users/Stepan/Tomita/Tomita-biz.proto");
        pb.directory(new File("/Users/Stepan/Tomita/"));
        Process p = pb.start();
        p.waitFor();
//
//        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
//        StringBuilder sb = new StringBuilder();
//        String line;
//        while ((line = br.readLine()) != null) {
//            sb.append(line).append("\n");
//        }
//        String message = sb.toString();
//        System.out.println(message);

    }

    public static void changeTomitaBizProtoSettings(String tomitaDirectoryIn, String tomitaDirectoryOut, String hotelName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/Tomita/Tomita-biz.proto"));
        String line;
        StringTokenizer tokenizer;
        boolean flagIn;
        boolean flagOut;

        StringBuilder builder = new StringBuilder();

        while ((line = br.readLine()) != null) {
            flagIn = false;
            flagOut = false;
            tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                String word = tokenizer.nextToken();
                if (word.equals("Input")) {
                    tokenizer = new StringTokenizer(br.readLine());

                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    builder.append("Input = { \n" + "File = ").append(tomitaDirectoryIn).append(hotelName).append("\"\n");
                    flagIn = true;

                }
                if (word.equals("Output")) {
                    tokenizer = new StringTokenizer(br.readLine());

                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    builder.append("Output = { \n" + "File = ").append(tomitaDirectoryOut).append("Tomita").append(hotelName).append("\"\n");
                    flagOut = true;

                }


            }
            if (!flagIn && !flagOut) {
                builder.append(line).append("\n");
            }

        }

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/Tomita/tomita-biz.proto"))) {
            out.print(builder.toString());
        }

    }
}
