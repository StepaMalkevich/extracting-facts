package exstractFacts;

import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 08.11.15.
 */
public class Facts {

    public static void main(String[] args) throws IOException {
//        getUniqueFactsFromReviews();
        getSomeFacts();
    }

    public static void getUniqueFactsFromReviews() throws IOException {
        String sampleNumber = "1";

        List<String> factsInBase = getFromDictionary();
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";


        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        for (Hotel h : hotels) {

            String name = h.getName().replaceAll(" ", "");

            Map<String, Map<String, Integer>> facts = getFactsFromTomitaOut(sampleNumber, "Tomita" + name);

            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {
                if (factsInBase.contains(entry.getKey())) {
                    System.out.println(entry.getKey() + " = " + entry.getValue());
                }
            }
        }


    }

    public static List<String> getFromDictionary() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/factDictionaryRus"));
        String line;
        List<String> dictionary = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            dictionary.add(line);
        }
        return dictionary;
    }

    public static Map<String, Map<String, Integer>> getFactsFromTomitaOut(String sampleNumber, String tomitaHotelName) throws IOException {
        Map<String, Map<String, Integer>> facts = new HashMap<>();

        BufferedReader br;
        if (sampleNumber.equals("1") || sampleNumber.equals("2")) {
            br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOut" + sampleNumber + "_top_100Sample/" + tomitaHotelName + ".out"));
        } else {
            br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOutFirstTrySample/" + tomitaHotelName + ".out"));
        }
        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {

            line = line.toLowerCase();// toLowerCase()


            Map<String, Integer> map = new HashMap<>();
            tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                String word = tokenizer.nextToken();
                String param;
                String opinion = "";
                String negative = "";
                if (word.equals("param")) {
                    tokenizer.nextToken();//прошли =
                    param = tokenizer.nextToken();

                    if (facts.containsKey(param)) {
                        map = facts.get(param);
                    }

                    //теперь нужно обработать случай с Negative_string = нет
                    String nextLine = br.readLine(); // toLowerCase()
                    nextLine = nextLine.toLowerCase();
                    if (nextLine.contains("opinion")) {
                        tokenizer = new StringTokenizer(nextLine);
                        tokenizer.nextToken();
                        tokenizer.nextToken();

                        while (tokenizer.hasMoreTokens()) {
                            opinion = opinion + tokenizer.nextToken();//без пробела
                        }

                        map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                        facts.put(param, map);
                    } else {
                        if (nextLine.contains("string")) {
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();
                            negative = tokenizer.nextToken();

                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();

                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();

                            while (tokenizer.hasMoreTokens()) {
                                opinion = negative + opinion + tokenizer.nextToken();//без пробела
                            }

                            map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                            facts.put(param, map);


                        } else {//может быть или только negative или и negative и negative_string
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            if (nextLine.contains("opinion")) {
                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();

                                while (tokenizer.hasMoreTokens()) {
                                    opinion = "true" + opinion + tokenizer.nextToken();//без пробела
                                }

                                map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                facts.put(param, map);
                            } else {

                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();
                                negative = tokenizer.nextToken();

                                nextLine = br.readLine();
                                nextLine = nextLine.toLowerCase();

                                if (nextLine.contains("opinion")) {
                                    tokenizer = new StringTokenizer(nextLine);
                                    tokenizer.nextToken();
                                    tokenizer.nextToken();

                                    while (tokenizer.hasMoreTokens()) {
                                        opinion = negative + opinion + tokenizer.nextToken();//без пробела
                                    }

                                    map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                    facts.put(param, map);
                                }
                            }


                        }

                    }

                }
            }
        }
        return facts;
    }


    public static Map<String, Map<String, Integer>> getFactsFromTomitaOutBooking(int bookingNumber, String tomitaHotelName) throws IOException {
        Map<String, Map<String, Integer>> facts = new HashMap<>();
        BufferedReader br;
        if (bookingNumber == -1) {
            br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOutBookingMyGrammar/" + tomitaHotelName + ".out"));
        } else {
            //(MyGrammarTransfer)
            br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOutBookingHotels" + bookingNumber + "/" + tomitaHotelName + ".out"));
        }
        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {

            line = line.toLowerCase();// toLowerCase()


            Map<String, Integer> map = new HashMap<>();
            tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                String word = tokenizer.nextToken();
                String param;
                String opinion = "";
                String negative = "";
                if (word.equals("param")) {
                    tokenizer.nextToken();//прошли =
                    param = tokenizer.nextToken();

                    if (facts.containsKey(param)) {
                        map = facts.get(param);
                    }

                    //теперь нужно обработать случай с Negative_string = нет
                    String nextLine = br.readLine(); // toLowerCase()
                    nextLine = nextLine.toLowerCase();
                    if (nextLine.contains("opinion")) {
                        tokenizer = new StringTokenizer(nextLine);
                        tokenizer.nextToken();
                        tokenizer.nextToken();

                        while (tokenizer.hasMoreTokens()) {
                            opinion = opinion + tokenizer.nextToken();//без пробела
                        }

                        map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                        facts.put(param, map);
                    } else {
                        if (nextLine.contains("string")) {
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();
                            negative = tokenizer.nextToken();

                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();

                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();

                            while (tokenizer.hasMoreTokens()) {
                                opinion = negative + opinion + tokenizer.nextToken();//без пробела
                            }

                            map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                            facts.put(param, map);


                        } else {//может быть или только negative или и negative и negative_string
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            if (nextLine.contains("opinion")) {
                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();

                                while (tokenizer.hasMoreTokens()) {
                                    opinion = "true" + opinion + tokenizer.nextToken();//без пробела
                                }

                                map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                facts.put(param, map);
                            } else {

                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();
                                negative = tokenizer.nextToken();

                                nextLine = br.readLine();
                                nextLine = nextLine.toLowerCase();

                                if (nextLine.contains("opinion")) {
                                    tokenizer = new StringTokenizer(nextLine);
                                    tokenizer.nextToken();
                                    tokenizer.nextToken();

                                    while (tokenizer.hasMoreTokens()) {
                                        opinion = negative + opinion + tokenizer.nextToken();//без пробела
                                    }

                                    map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                    facts.put(param, map);
                                }
                            }


                        }

                    }

                }
            }
        }
        return facts;
    }


    public static Map<String, Map<String, Integer>> getRandomNumberOfFactsFromTomitaOut(int cntOfReviews, int numberOfReviews, String sampleNumber, String tomitaHotelName) throws IOException {
        Map<String, Map<String, Integer>> facts = new HashMap<>();
        Random random = new Random();
        Set<Integer> set = new HashSet<>();
        boolean flag = false;

        if (cntOfReviews <= numberOfReviews) {
            flag = true;
        }

        if (!flag) {
            int n = 100000;
            while (n > 0 && numberOfReviews > 0) {
                int r = random.nextInt(cntOfReviews);
                if (!set.contains(r)) {
                    set.add(r);
                    numberOfReviews--;
                }
                n--;
            }

            if (n == 0) {
                System.out.println("zero");
            }
        }
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/forRandomRewievsTomitaOut/tomitaOut" + sampleNumber + "_top_100/" + tomitaHotelName + ".out"));
        String line;
        StringTokenizer tokenizer;
        int cntOfBeginLine = 0;

        while ((line = br.readLine()) != null) {
            line = line.toLowerCase();// toLowerCase()

            if (!flag) {
                if (line.contains("beginline")) {
                    cntOfBeginLine++;
                }

                if (!set.contains(cntOfBeginLine)) {
                    continue;
                }
            }
            Map<String, Integer> map = new HashMap<>();
            tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                String word = tokenizer.nextToken();
                String param;
                String opinion = "";
                String negative = "";
                if (word.equals("param")) {
                    tokenizer.nextToken();//прошли =
                    param = tokenizer.nextToken();

                    if (facts.containsKey(param)) {
                        map = facts.get(param);
                    }

                    //теперь нужно обработать случай с Negative_string = нет
                    String nextLine = br.readLine(); // toLowerCase()
                    nextLine = nextLine.toLowerCase();
                    if (nextLine.contains("opinion")) {
                        tokenizer = new StringTokenizer(nextLine);
                        tokenizer.nextToken();
                        tokenizer.nextToken();

                        while (tokenizer.hasMoreTokens()) {
                            opinion = opinion + tokenizer.nextToken();//без пробела
                        }

                        map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                        facts.put(param, map);
                    } else {
                        if (nextLine.contains("string")) {
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();
                            negative = tokenizer.nextToken();

                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();

                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();

                            while (tokenizer.hasMoreTokens()) {
                                opinion = negative + opinion + tokenizer.nextToken();//без пробела
                            }

                            map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                            facts.put(param, map);


                        } else {//может быть или только negative или и negative и negative_string
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            if (nextLine.contains("opinion")) {
                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();

                                while (tokenizer.hasMoreTokens()) {
                                    opinion = "true" + opinion + tokenizer.nextToken();//без пробела
                                }

                                map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                facts.put(param, map);
                            } else {

                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();
                                negative = tokenizer.nextToken();

                                nextLine = br.readLine();
                                nextLine = nextLine.toLowerCase();

                                if (nextLine.contains("opinion")) {
                                    tokenizer = new StringTokenizer(nextLine);
                                    tokenizer.nextToken();
                                    tokenizer.nextToken();

                                    while (tokenizer.hasMoreTokens()) {
                                        opinion = negative + opinion + tokenizer.nextToken();//без пробела
                                    }

                                    map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                    facts.put(param, map);
                                }
                            }


                        }

                    }

                }
            }
        }
        return facts;
    }


    public static Map<String, Map<String, Integer>> getFirstNumberOfFactsFromTomitaOut(int numberOfReviews, String sampleNumber, String tomitaHotelName) throws IOException {
        Map<String, Map<String, Integer>> facts = new HashMap<>();
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/forRandomRewievsTomitaOut/tomitaOut" + sampleNumber + "_top_100/" + tomitaHotelName + ".out"));
        String line;
        StringTokenizer tokenizer;

        while ((line = br.readLine()) != null) {
            line = line.toLowerCase();// toLowerCase()

            numberOfReviews--;

            if (numberOfReviews <=0){
                break;
            }

            Map<String, Integer> map = new HashMap<>();
            tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                String word = tokenizer.nextToken();
                String param;
                String opinion = "";
                String negative = "";
                if (word.equals("param")) {
                    tokenizer.nextToken();//прошли =
                    param = tokenizer.nextToken();

                    if (facts.containsKey(param)) {
                        map = facts.get(param);
                    }

                    //теперь нужно обработать случай с Negative_string = нет
                    String nextLine = br.readLine(); // toLowerCase()
                    nextLine = nextLine.toLowerCase();
                    if (nextLine.contains("opinion")) {
                        tokenizer = new StringTokenizer(nextLine);
                        tokenizer.nextToken();
                        tokenizer.nextToken();

                        while (tokenizer.hasMoreTokens()) {
                            opinion = opinion + tokenizer.nextToken();//без пробела
                        }

                        map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                        facts.put(param, map);
                    } else {
                        if (nextLine.contains("string")) {
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();
                            negative = tokenizer.nextToken();

                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();

                            tokenizer = new StringTokenizer(nextLine);
                            tokenizer.nextToken();
                            tokenizer.nextToken();

                            while (tokenizer.hasMoreTokens()) {
                                opinion = negative + opinion + tokenizer.nextToken();//без пробела
                            }

                            map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                            facts.put(param, map);


                        } else {//может быть или только negative или и negative и negative_string
                            nextLine = br.readLine();
                            nextLine = nextLine.toLowerCase();
                            if (nextLine.contains("opinion")) {
                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();

                                while (tokenizer.hasMoreTokens()) {
                                    opinion = "true" + opinion + tokenizer.nextToken();//без пробела
                                }

                                map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                facts.put(param, map);
                            } else {

                                tokenizer = new StringTokenizer(nextLine);
                                tokenizer.nextToken();
                                tokenizer.nextToken();
                                negative = tokenizer.nextToken();

                                nextLine = br.readLine();
                                nextLine = nextLine.toLowerCase();

                                if (nextLine.contains("opinion")) {
                                    tokenizer = new StringTokenizer(nextLine);
                                    tokenizer.nextToken();
                                    tokenizer.nextToken();

                                    while (tokenizer.hasMoreTokens()) {
                                        opinion = negative + opinion + tokenizer.nextToken();//без пробела
                                    }

                                    map.compute(opinion, (w, f) -> f == null ? 1 : f + 1);
                                    facts.put(param, map);
                                }
                            }


                        }

                    }

                }
            }
        }
        return facts;
    }

    public static void getDictionary() throws IOException {
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/first100.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        List<String> uniqueFeatures = new ArrayList<>();

        for (Hotel h : hotels) {
            for (String f : h.getFeatures()) {
                String[] p = f.split("=");
                if (!uniqueFeatures.contains(p[0])) {
                    uniqueFeatures.add(p[0]);
                }
            }
        }

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/factDictionary"))) {
            for (String tmp : uniqueFeatures) {
                out.println(tmp);
            }
        }


    }


    public static void getDictionaryFact_Value() throws IOException {
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/2_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        List<String> uniqueFeatures = new ArrayList<>();

        int ccPrivate = 0;
        int ccPublic = 0;
        for (Hotel h : hotels) {
            boolean flagPublic = false;
            boolean flagPrivate = false;
            for (String f : h.getFeatures()) {
                if (!uniqueFeatures.contains(f)) {
                    uniqueFeatures.add(f);
                }
                if (f.equals("public_beach=true")) {
                    flagPublic = true;
                }

                if (f.equals("private_beach=true")) {
                    flagPrivate = true;
                }

            }

            if (!flagPrivate) {
                ccPrivate++;
            }

            if (!flagPublic) {
                ccPublic++;
            }


        }
        System.out.println("number of hotels without fact public_beach=true is " + ccPublic);
        System.out.println("number of hotels without fact private_beach=true is " + ccPrivate);

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/factDictionaryFact_Value"))) {
            for (String tmp : uniqueFeatures) {
                out.println(tmp);
            }
        }


    }

    public static void getSomeFacts() throws IOException {
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/2_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        List<String> uniqueFeatures = new ArrayList<>();

        int ccPrivate = 0;
        int ccPublic = 0;
        for (Hotel h : hotels) {
            for (String f : h.getFeatures()) {
                StringTokenizer tokenizer = new StringTokenizer(f, "=");
                String elem = tokenizer.nextToken();
                if (!uniqueFeatures.contains(elem)) {
                    uniqueFeatures.add(elem);
                }
            }

        }

        uniqueFeatures.forEach(i -> System.out.println(i));

    }


}
