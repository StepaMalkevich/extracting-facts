package metric;

import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;
import exstractFacts.Facts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 25.11.15.
 */
public class MetricForWiFi {

    static Map<String, List<String>> columnBaseAlgorithm = new HashMap<>();

    public static void main(String[] args) throws IOException {
        String sampleNumber = "3";
        getMetricsRes(sampleNumber);
    }

    public static void getMetricsRes(String sampleNumber) throws IOException {

        String hotelsSampleFile;

        if (sampleNumber.equals("1") || sampleNumber.equals("2")) {
            hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";
        } else {
            hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/first100.tsv";
        }

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        Map<String, String> table = getTable();

        final String fact = "paid";

        BufferedReader br;
        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;

        for (Hotel h : hotels) {

            String valueInBase = "nothing";
            String info = "";
            double free = 0;
            double paid = 0;


            String name = h.getName().replaceAll(" ", "");

            if (name.equals("ХилтонХургадаРезорт")) {
                //тут false url
                falseUrlCount++;
                continue;
            }


            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);
            } catch (IOException e) {
                noReviews++; // нет отелей => нет отзывов
                continue;
            }

            double allFacts = facts.size();
            info = info + "allFacts = " + allFacts + "\n";

            if (sampleNumber.equals("1") || sampleNumber.equals("2")) {
                br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/hotelFeatures/" + sampleNumber + "_top_100Features/feature" + name + ".txt"));
            } else {
                br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/hotelFeatures/firstTryFeatures(First100FromFile)/feature" + name + ".txt"));
            }

            String line;
            while ((line = br.readLine()) != null) {
                if (line.matches(".*_wi_fi_in_public_areas")) {
                    valueInBase = line;
                }
            }

            if (valueInBase.equals("nothing")) {
                System.out.println("nothing about free/paid wi-fi in base " + name);
            }

            String base;

            switch (valueInBase) {
                case "internet_in_hotel=free_wi_fi_in_public_areas":
                    base = "free";
                    break;
                case "internet_in_hotel=paid_wi_fi_in_public_areas":
                    base = "paid";
                    break;
                case "nothing":
                    base = "nothing";
                    break;
                default:
                    throw new IllegalStateException();

            }


            info = info + "base : " + base + "\n";

            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                for (String keyWord : Arrays.asList("wi-fi", "сеть", "вай-фай", "беспроводной", "wi fi", "вай фай", "вай файв", "ва фай", "ви фи", "фай вай",
                        "wireless", "беспроводная сеть", "ви-фи", "wan", "вафушка", "хай-фай", "вафля", "интернет", "беспроводной интернет")) {

                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                            String s = keyWord + "=" + in.getKey();
                            if (table.containsKey(s)) {
                                info = info + "reviews : " + s + "     " + table.get(s) + " count = " + in.getValue() + "\n";

                                if (table.get(s).equals("internet_in_hotel=free_wi_fi_in_public_areas")) {
                                    free += in.getValue();
                                } else {
                                    if (table.get(s).equals("internet_in_hotel=paid_wi_fi_in_public_areas")) {
                                        paid += in.getValue();
                                    }
                                }
                            }


                        }
                    }

                }

            }


            info = info + free + " " + paid + "\n";

            String conclusionFromReviews = getConclusion(1 * free, 1 * paid);

            info = info + "conclusion = " + conclusionFromReviews + "\n";

            if (free == paid && free == 0) {
                noReviews++;
                System.out.println("noReviews  " + h.getName());
            } else {
                if (base.equals("nothing")) {
                    noValuesInBase++;
                    System.out.println(name + " --- NothingInBase");
                    System.out.println(info);
                } else {
                    if (base.equals(fact) && conclusionFromReviews.contains(fact)) {
                        truePositive++;
                    } else {
                        if (!base.equals(fact) && (!conclusionFromReviews.contains(fact) || conclusionFromReviews.length() > 4)) {
                            trueNegative++;
                        } else {
                            if (base.equals(fact) && !conclusionFromReviews.contains(fact)) {
                                falseNegative++;
                                System.out.println(name + " --- falseNegative");
                                System.out.println(info);

                            } else {
                                if (!base.equals(fact) && conclusionFromReviews.contains(fact)) {
                                    falsePositive++;
                                    System.out.println(name + " --- falsePositive");
                                    System.out.println(info);
                                }
                            }
                        }
                    }
                }
            }

            columnBaseAlgorithm.put(name, Arrays.asList(base, conclusionFromReviews, String.valueOf(free), String.valueOf(paid)));

        }

        double ACC = (truePositive + trueNegative) / (truePositive + trueNegative + falsePositive + falseNegative);

        double precision = truePositive / (truePositive + falsePositive);

        double recall = truePositive / (truePositive + falseNegative);

        System.out.println("truePositive = " + truePositive);
        System.out.println("trueNegative = " + trueNegative);
        System.out.println("falsePositive = " + falsePositive);
        System.out.println("falseNegative = " + falseNegative);

        System.out.println("accuracy = " + ACC);
        System.out.println("precision = " + precision);
        System.out.println("recall = " + recall);

        System.out.println("number of hotels without fact=wi-fi free/paid in reviews = " + noReviews);
        System.out.println("number of hotels without fact=wi-fi free/paid in base = " + noValuesInBase);
        System.out.println("number of hotels with false url = " + falseUrlCount);

        System.out.println("allCount = " + (truePositive + trueNegative + falsePositive + falseNegative + noReviews + noValuesInBase + falseUrlCount));


    }

    public static Map<String, List<String>> getColumnList(String sampleNumber) throws IOException {
        getMetricsRes(sampleNumber);
        return columnBaseAlgorithm;
    }


    private static String getConclusion(double countFree, double countPaid) {
        String conclusion;

        //этот if означает, что если так, то я соглашаюсь с базой и неважно, что там free или paid
        if (Math.abs(countFree - countPaid) <= 2) {
            conclusion = "free paid";
            return conclusion;
        }
        if (countFree > countPaid) {
            return "free";
        } else {
            return "paid";
        }
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/wi-fiFree.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }


        return map;
    }
}
