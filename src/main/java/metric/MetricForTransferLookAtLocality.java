package metric;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

/**
 * Created by Stepan on 15.03.16.
 */
public class MetricForTransferLookAtLocality {
    static int width = 10;
    static int count = 10;


    public static void main(String[] args) throws IOException {
        String sampleNumber = "2";
        List<String> randomSampleName = MetricForTransfer.getRandomSampleName(sampleNumber);
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            int r = random.nextInt(randomSampleName.size());
            String hotel = randomSampleName.get(r);
            String[] p = hotel.split(" ");

            String name = p[0];
            String tomitaHotelName = "Tomita" + name;
            String feature = p[1];
            System.out.println("----" + name + "----" + feature);


            BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reviews/" + sampleNumber + "_top_100Sample/" + name + ".out"));
//            BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOut" + sampleNumber + "_top_100Sample/" + tomitaHotelName + ".out"));
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains(feature)) {
                    StringTokenizer tokenizer = new StringTokenizer(line);
                    StringBuilder builderPhraseFeatures = new StringBuilder();
                    int index = 0;
                    while (tokenizer.hasMoreTokens()) {

                        String word = tokenizer.nextToken();
                        index++;

                        if (word.contains(feature)) {
                            //предыдущие width слов
                            StringTokenizer tokenizerSkip = new StringTokenizer(line);
                            for (int j = 0; j < index - width; j++) {
                                tokenizerSkip.nextToken();
                            }
                            for (int j = 0; j < width - 1; j++) {
                                builderPhraseFeatures.append(tokenizerSkip.nextToken()).append(" ");
                            }

                            int j = 0;
                            builderPhraseFeatures.append(word).append(" ");
                            //следующие width слов
                            while (j < width && tokenizer.hasMoreTokens()) {
                                builderPhraseFeatures.append(tokenizer.nextToken()).append(" ");
                                j++;
                            }

                        }
                    }
                    System.out.println(builderPhraseFeatures.toString());
                }
            }

            System.out.println();
        }

    }
}
