package metric;

import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;
import exstractFacts.Facts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 14.11.15.
 */
public class MetricForBeach {

    static final double limit = 0.6;
    static Map<String, List<String>> columnBaseAlgorithm = new HashMap<>();
    static List<String> randomSampleName = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        getMetricsRes();
    }

    public static void getMetricsRes() throws IOException {

        String sampleNumber = "2";
        String hotelsSampleFile;

        if (sampleNumber.equals("1") || sampleNumber.equals("2")) {
            hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";
        } else {
            hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/first100.tsv";
        }

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        Map<String, String> table = getTable();
        List<String> listKeyWords = Arrays.asList("пляж", "песок", "заход", "дно");

        String fact = "mixed";

        BufferedReader br;
        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;

        List<String> hotelsWithoutReviews = Arrays.asList("CesarsTempleDeLuxeHotel", "ClubSeaTimeHotel", "GrandSunlifeHotel", "FestivalRivieraResort", "LarissaHolidayBeachClub", "ClubOasisEternity(Ex.McsOasisBeachClubHotel)", "InovaBeachHotel", "RadissonBluResort");

        for (Hotel h : hotels) {
            //false url!!!

            String valueInBase = "nothing";
            String info = "";
            double countSand = 0;
            double countPebble = 0;
            double countMixed = 0;


            String name = h.getName().replaceAll(" ", "");

            if (hotelsWithoutReviews.contains(name)) {
                falseUrlCount++;
                continue;
            }


            //почему-то некоторых отелей нет
            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);
            } catch (IOException e) {
                noReviews++; // нет отелей => нет отзывов
                continue;
            }

            double allFacts = facts.size();
            info = info + "allFacts = " + allFacts + "\n";

            if (sampleNumber.equals("1") || sampleNumber.equals("2")) {
                br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/hotelFeatures/" + sampleNumber + "_top_100Features/feature" + name + ".txt"));
            } else {
                br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/hotelFeatures/firstTryFeatures(First100FromFile)/feature" + name + ".txt"));
            }
            String line;
            while ((line = br.readLine()) != null) {
                if (line.matches("^beach=.*")) {
                    valueInBase = line;
                }
            }

            if (valueInBase.equals("nothing")) {
                noValuesInBase++;
            }

            String base;

            switch (valueInBase) {
                case "beach=sand":
                    base = "sand";
                    break;
                case "beach=pebble":
                    base = "pebble";
                    break;
                case "beach=mixed_sand_pebble":
                    base = "mixed";
                    break;
                default:
                    throw new IllegalStateException();

            }

            info = info + "base : " + valueInBase + "\n";


            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {


                for (String keyWord : listKeyWords) {
                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                            // тут нужно достать in.getKey() in.getValue() <-тогда слово встречается в отзывах не 1 раз, а in.getValue()
                            String s = keyWord + "=" + in.getKey();
                            if (table.containsKey(s)) {
                                //нужно пойти и посмотерть чему равно значение в базе и сравнить его с table.get(s)
                                //руководствуемся мнением большинства
                                info = info + "reviews : " + table.get(s) + " count = " + in.getValue() + "\n";

                                if (table.get(s).equals("beach=sand")) {
                                    countSand += in.getValue();
                                } else {
                                    if (table.get(s).equals("beach=pebble")) {
                                        countPebble += in.getValue();
                                    } else {
                                        if (table.get(s).equals("beach=mixed_sand_pebble")) {
                                            countMixed += in.getValue();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                //добавил новые слова, описывающие факт пляж=песчаный
                if (entry.getKey().equals("галька")) {
                    int cc = entry.getValue().size();
                    countPebble += cc;
                    info = info + "reviews : beach=pebble count = " + cc + "\n";
                    randomSampleName.add(name + " галька");
                }
                /*
                пересмотрел факты, связанные со словом песок
                if (entry.getKey().equals("песок")) {
                    int cc = entry.getValue().size();
                    countSand += cc;
                    info = info + "reviews : beach=sand count = " + cc + "\n";
                    randomSampleName.add(name + " песок");
                }*/


            }


            info = info + countSand + " " + countPebble + " " + countMixed + "\n";

            //тут нужно сделать вывод о состоянии пляжа из отзывов
            String conclusionFromReviews = getConclusion(1 * countSand, 2 * countPebble, 3 * countMixed); //поробуем веса 1 2 3

            info = info + "conclusion = " + conclusionFromReviews + "\n";

            if (countSand == countPebble && countMixed == countSand && countSand == 0) {
                System.out.println(name + " noReviews!!!");
                conclusionFromReviews = "noReviews";
                noReviews++;
            } else {
                if (base.equals(fact) && conclusionFromReviews.contains(fact)) {
                    truePositive++;
                } else {
                    if (base.equals("nothing")) {
                        noValuesInBase++;
                        System.out.println(name + " --- NothingInBase");
                        System.out.println(info);
                    } else {

                        //либо review не содержит sand либо содержит не только sand
                        if (!base.equals(fact) && (!conclusionFromReviews.contains(fact) || conclusionFromReviews.length() > 4)) {
                            trueNegative++;

//                        System.out.println(name + " --- trueNegative");
//                        System.out.println(info);


                        } else {
                            if (base.equals(fact) && !conclusionFromReviews.contains(fact)) {
                                falseNegative++;
                                System.out.println(name + " --- falseNegative");

                                if (countPebble / (countSand + countMixed + countPebble) > limit || countMixed / (countSand + countMixed + countPebble) > limit) {
                                    System.out.println("Warning!!! Error in base");
                                }

                                System.out.println(info);


                            } else {
                                if (!base.equals(fact) && conclusionFromReviews.contains(fact)) {
                                    falsePositive++;
                                    System.out.println(name + " --- falsePositive");

                                    if (countSand / (countSand + countMixed + countPebble) > limit) {
                                        System.out.println("Warning!!! Error in base");
                                    }

                                    System.out.println(info);

                                }
                            }
                        }
                    }
                }
            }


            //это для xls
            columnBaseAlgorithm.put(name, Arrays.asList(base, conclusionFromReviews));

        }


        double ACC = (truePositive + trueNegative) / (truePositive + trueNegative + falsePositive + falseNegative);

        double precision = truePositive / (truePositive + falsePositive);

        double recall = truePositive / (truePositive + falseNegative);

        System.out.println("truePositive = " + truePositive);
        System.out.println("trueNegative = " + trueNegative);
        System.out.println("falsePositive = " + falsePositive);
        System.out.println("falseNegative = " + falseNegative);

        System.out.println("accuracy = " + ACC);
        System.out.println("precision = " + precision);
        System.out.println("recall = " + recall);

        System.out.println("number of hotels without fact=beach sand/pebble/mixed in reviews = " + noReviews);
        System.out.println("number of hotels without fact=beach sand/pebble/mixed  in base = " + noValuesInBase);
        System.out.println("number of hotels with false url = " + falseUrlCount);

        System.out.println("allCount = " + (truePositive + trueNegative + falsePositive + falseNegative + noReviews + noValuesInBase + falseUrlCount));


    }

    public static Map<String, List<String>> getColumnList() throws IOException {
        getMetricsRes();
        return columnBaseAlgorithm;
    }

    public static List<String> getRandomSampleName() throws IOException {
        getMetricsRes();
        return randomSampleName;
    }


    private static String getConclusion(double countSand, double countPebble, double countMixed) {
        String conclusion;
        if (countSand == countPebble && countSand == countMixed) {
            conclusion = "sand pebble mixed";
            return conclusion;
        }

        if (Math.abs(countSand - countPebble) <= 3) {
            conclusion = "sand pebble mixed";
            return conclusion;
        }


        if (countSand == countMixed && countPebble > countSand) {
            conclusion = "pebble";
            return conclusion;
        }
        if (countSand == countMixed && countPebble < countSand) {
            conclusion = "sand mixed";
            return conclusion;
        }

        if (countMixed == countPebble && countSand > countMixed) {
            conclusion = "sand";
            return conclusion;
        }
        if (countMixed == countPebble && countSand < countMixed) {
            conclusion = "mixed pebble";
            return conclusion;
        }
        double max = Math.max(countSand, Math.max(countMixed, countPebble));
        if (max == countSand) {
            return "sand";
        }
        if (max == countPebble) {
            return "pebble";
        }
        if (max == countMixed) {
            return "mixed";
        }
        throw new IllegalStateException();
    }


    private static String getConclusionOnlyForSand(double countSand, double countPebble, double countMixed) {
        String conclusion;
        if (countSand == countPebble && countSand == countMixed) {
            conclusion = "sand pebble mixed";
            return conclusion;
        }

        if (countSand > 5 * countPebble && countSand > 5 * countMixed) {
            conclusion = "sand";
            return conclusion;
        }

        return null;
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/beach.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }


        return map;
    }
}

