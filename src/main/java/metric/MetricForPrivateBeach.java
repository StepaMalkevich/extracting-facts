package metric;

import exstractFacts.Facts;
import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 06.03.16.
 */
public class MetricForPrivateBeach {
    static Map<String, List<String>> columnBaseAlgorithm = new HashMap<>();

    public static void main(String[] args) throws IOException {
        String sampleNumber = "3";
        getMetricsRes(sampleNumber);
    }

    public static void getMetricsRes(String sampleNumber) throws IOException {

        String hotelsSampleFile;

        if (sampleNumber.equals("1") || sampleNumber.equals("2")) {
            hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";
        } else {
            hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/first100.tsv";
        }

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        Map<String, String> table = getTable();

        String fact = "public";

        BufferedReader br;
        double trueNegative = 0;
        double truePositive = 0;
        double falseNegative = 0;
        double falsePositive = 0;
        int noReviews = 0;
        int noValuesInBase = 0;
        int falseUrlCount = 0;

        for (Hotel h : hotels) {
            String valueInBase = "nothing";
            String info = "";
            double privateBeach = 0;
            double publicBeach = 0;


            String name = h.getName().replaceAll(" ", "");

            if (name.equals("ХилтонХургадаРезорт")) {
                //тут false url
                falseUrlCount++;
                continue;
            }


            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);
            } catch (IOException e) {
                noReviews++; // нет отелей => нет отзывов
                continue;
            }

            double allFacts = facts.size();
            info = info + "allFacts = " + allFacts + "\n";

            if (sampleNumber.equals("1") || sampleNumber.equals("2")) {
                br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/hotelFeatures/" + sampleNumber + "_top_100Features/feature" + name + ".txt"));
            } else {
                br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/hotelFeatures/firstTryFeatures(First100FromFile)/feature" + name + ".txt"));
            }

            String line;
            while ((line = br.readLine()) != null) {
                if (line.matches(".*_beach=true")) {
                    valueInBase = line;
                }
            }


            if (valueInBase.equals("nothing")) {
                System.out.println("nothing about private/public wi-fi in" + name);
                noValuesInBase++;
            }

            String base;

            try {
                switch (valueInBase) {
                    case "private_beach=true":
                        base = "private";
                        break;
                    case "public_beach=true":
                        base = "public";
                        break;
                    default:
                        throw new IllegalStateException();

                }
            } catch (IllegalStateException e){
                continue;
            }

            info = info + "base : " + base + "\n";

            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                for (String keyWord : Arrays.asList("пляж")) {

                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                            String s = keyWord + "=" + in.getKey();
                            if (table.containsKey(s)) {
                                info = info + "reviews : " + s + "     " + table.get(s) + " count = " + in.getValue() + "\n";

                                if (table.get(s).equals("private_beach=true")) {
                                    privateBeach += in.getValue();
                                } else {
                                    if (table.get(s).equals("public_beach=true")) {
                                        publicBeach += in.getValue();
                                    }
                                }
                            }


                        }
                    }

                }

            }


            info = info + privateBeach + " " + publicBeach + "\n";

            String conclusionFromReviews = getConclusion(1 * privateBeach, 1 * publicBeach);

            info = info + "conclusion = " + conclusionFromReviews + "\n";

            if (privateBeach == publicBeach && publicBeach == 0) {
                noReviews++;
                System.out.println("noReviews  " + h.getName());
            } else {
                if (base.equals("nothing")) {
                    noValuesInBase++;
                    System.out.println(name + " --- NothingInBase");
                    System.out.println(info);
                } else {
                    if (base.equals(fact) && conclusionFromReviews.contains(fact)) {
                        truePositive++;
                    } else {
                        if (!base.equals(fact) && (!conclusionFromReviews.contains(fact) || conclusionFromReviews.length() > 4)) {
                            trueNegative++;
                        } else {
                            if (base.equals(fact) && !conclusionFromReviews.contains(fact)) {
                                falseNegative++;
                                System.out.println(name + " --- falseNegative");
                                System.out.println(info);

                            } else {
                                if (!base.equals(fact) && conclusionFromReviews.contains(fact)) {
                                    falsePositive++;
                                    System.out.println(name + " --- falsePositive");
                                    System.out.println(info);
                                }
                            }
                        }
                    }
                }
            }

            columnBaseAlgorithm.put(name, Arrays.asList(base, conclusionFromReviews, String.valueOf(privateBeach), String.valueOf(publicBeach)));

        }

        double ACC = (truePositive + trueNegative) / (truePositive + trueNegative + falsePositive + falseNegative);

        double precision = truePositive / (truePositive + falsePositive);

        double recall = truePositive / (truePositive + falseNegative);

        System.out.println("truePositive = " + truePositive);
        System.out.println("trueNegative = " + trueNegative);
        System.out.println("falsePositive = " + falsePositive);
        System.out.println("falseNegative = " + falseNegative);

        System.out.println("accuracy = " + ACC);
        System.out.println("precision = " + precision);
        System.out.println("recall = " + recall);

        System.out.println("number of hotels without fact=wi-fi free/paid in reviews = " + noReviews);
        System.out.println("number of hotels without fact=wi-fi free/paid in base = " + noValuesInBase);
        System.out.println("number of hotels with false url = " + falseUrlCount);

        System.out.println("allCount = " + (truePositive + trueNegative + falsePositive + falseNegative + noReviews + noValuesInBase + falseUrlCount));


    }

    public static Map<String, List<String>> getColumnList(String sampleNumber) throws IOException {
        getMetricsRes(sampleNumber);
        return columnBaseAlgorithm;
    }


    private static String getConclusion(double countPrivate, double countPublic) {
        String conclusion;

        //этот if означает, что если так, то я соглашаюсь с базой и неважно, что там free или paid
        if (Math.abs(countPrivate - countPublic) == 0) {
            conclusion = "private public";
            return conclusion;
        }
        if (countPrivate > countPublic) {
            return "private";
        } else {
            return "public";
        }
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/privateBeach.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }


        return map;
    }
}
