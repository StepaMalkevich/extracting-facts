package metric.dataToPlot;

import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.*;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Stepan on 18.05.16.
 */
public class RandomRewivesThroughTomita {

    public static void main(String[] args) throws IOException, InterruptedException {
//        addBeginEndLine();
//        startTripAdvisor();
        double truePositive = 13 + 13;
        double trueNegative = 49 + 35;

        double falsePositive = 0 + 0;
        double falseNegative = 3 + 21;

        double ACC = (truePositive + trueNegative) / (truePositive + trueNegative + falsePositive + falseNegative);

        double precision = truePositive / (truePositive + falsePositive);

        double recall = truePositive / (truePositive + falseNegative);

        System.out.println(ACC);
        System.out.println(precision);
        System.out.println(recall);

    }


    public static void addBeginEndLine() throws IOException {
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/2_top_100Hotels.tsv";
        String tomitaDirectoryOut = "/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/tomitaOut2_top_100Sample/";

        String reviewsOut = "/Users/Stepan/malkevich.stepan/extracting-facts/reviews/2_top_100Sample/";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        for (Hotel h : hotels) {
            String name = h.getName();
            String fileName = name.replaceAll(" ", "");
            try {
                BufferedReader br = new BufferedReader(new FileReader(reviewsOut + fileName + ".out"));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append("beginline ").append(line).append(" endline").append("\n");
                }

                try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/forRandomRewievsTomitaOut/reviews2_top_100/" + fileName + ".out"))) {
                    out.print(builder.toString());
                }


            } catch (Exception e) {
                System.out.println("exp");
            }
        }
    }

    public static void startTripAdvisor() throws IOException, InterruptedException {
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/2_top_100Hotels.tsv";
        String tomitaDirectoryIn = "\"/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/forRandomRewievsTomitaOut/reviews2_top_100/";
        String tomitaDirectoryOut = "\"/Users/Stepan/malkevich.stepan/extracting-facts/tomitaOut/forRandomRewievsTomitaOut/tomitaOut2_top_100/";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        for (Hotel h : hotels) {
            String name = h.getName();
            String fileName = name.replaceAll(" ", "");

            System.out.println(name);

            tomitaMacExecute(tomitaDirectoryIn, tomitaDirectoryOut, fileName + ".out");
        }
    }

    public static void tomitaMacExecute(String tomitaDirectoryIn, String tomitaDirectoryOut, String hotelName) throws IOException, InterruptedException {
        changeTomitaBizProtoSettings(tomitaDirectoryIn, tomitaDirectoryOut, hotelName);
        tomitaRun();
    }

    public static void tomitaRun() throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder("/Users/Stepan/Tomita/Tomita-mac", "/Users/Stepan/Tomita/Tomita-biz.proto");
        pb.directory(new File("/Users/Stepan/Tomita/"));
        Process p = pb.start();
        p.waitFor();
    }

    public static void changeTomitaBizProtoSettings(String tomitaDirectoryIn, String tomitaDirectoryOut, String hotelName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/Tomita/tomita-biz.proto"));
        String line;
        StringTokenizer tokenizer;
        boolean flagIn;
        boolean flagOut;

        StringBuilder builder = new StringBuilder();

        while ((line = br.readLine()) != null) {
            flagIn = false;
            flagOut = false;
            tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                String word = tokenizer.nextToken();
                if (word.equals("Input")) {
                    tokenizer = new StringTokenizer(br.readLine());

                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    builder.append("Input = { \n" + "File = ").append(tomitaDirectoryIn).append(hotelName).append("\"\n");
                    flagIn = true;

                }
                if (word.equals("Output")) {
                    tokenizer = new StringTokenizer(br.readLine());

                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    tokenizer.nextToken();
                    builder.append("Output = { \n" + "File = ").append(tomitaDirectoryOut).append("Tomita").append(hotelName).append("\"\n");
                    flagOut = true;

                }


            }
            if (!flagIn && !flagOut) {
                builder.append(line).append("\n");
            }

        }

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/Tomita/Tomita-biz.proto"))) {
            out.print(builder.toString());
        }

    }
}
