package metric.dataToPlot;

import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Stepan on 19.05.16.
 */
public class WorkWithData {
    public static void main(String[] args) throws IOException {
        String sampleNumber = "1";
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        List<Integer> list = new ArrayList<>();


        for (Hotel h : hotels) {
            int cnt = 0;
            String name = h.getName().replaceAll(" ", "");
            try {
                BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reviews/" + sampleNumber + "_top_100Sample/" + name + ".out"));
                while (br.readLine()!=null){
                    cnt++;
                }

                list.add(cnt);

            } catch (FileNotFoundException e){
                System.out.println("exp");
            }
        }


        Collections.sort(list);
        System.out.println(list);

        System.out.println(list.get(list.size() / 2));

    }
}
