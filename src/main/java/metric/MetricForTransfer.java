package metric;

import exstractFacts.Facts;
import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 09.03.16.
 */
public class MetricForTransfer {


    static Map<String, List<String>> columnBaseAlgorithm = new HashMap<>();
    static List<String> randomSampleName = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        String sampleNumber = "2";
        getMetricsRes(sampleNumber);
    }


    public static void getMetricsRes(String sampleNumber) throws IOException {

        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);

        Map<String, Integer> beachMap = new HashMap<>();
        Map<String, Integer> aeroMap = new HashMap<>();
        Map<String, Integer> hotelMap = new HashMap<>();

        for (Hotel h : hotels) {

            String name = h.getName().replaceAll(" ", "");

            if (name.equals("ХилтонХургадаРезорт")) {
                //тут false url
                continue;
            }


            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);
            } catch (IOException e) {
                continue;
            }


//            System.out.println(name);

            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                for (String keyWord : Arrays.asList("трансфер")) {

                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
//                            System.out.println(entry.getKey() + "=" + in.getKey());
                            if (in.getKey().contains("пляж")) {
                                beachMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                            if (in.getKey().contains("аэро")) {
                                aeroMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                            if (in.getKey().contains("отел")) {
                                hotelMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                        }

                        randomSampleName.add(name + " трансфер");
                    }

                }

            }

        }


        System.out.println("трансфер до пляжа\nколичество отелей из 100 = "
                + beachMap.size() + "\nколичество отелей, в которых на самом деле пишут про трансфер до пляжа = ?" + "\n" + beachMap + "\n");
        System.out.println("трансфер до аэропорта\nколичество отелей из 100 = " +
                aeroMap.size() + "\nколичество отелей, в которых на самом деле пишут про трансфер до аэропорта = ?"+ "\n" + aeroMap + "\n");
        System.out.println("трансфер до отеля\nколичество отелей из 100 = " +
                hotelMap.size() + "\nколичество отелей, в которых на самом деле пишут про трансфер до отеля = ?" + "\n" + hotelMap + "\n");


    }

    public static Map<String, List<String>> getColumnList(String sampleNumber) throws IOException {
        getMetricsRes(sampleNumber);
        return columnBaseAlgorithm;
    }

    public static List<String> getRandomSampleName(String sampleNumber) throws IOException {
        getMetricsRes(sampleNumber);
        return randomSampleName;
    }

}
