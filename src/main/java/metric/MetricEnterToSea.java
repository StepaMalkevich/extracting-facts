package metric;

import exstractFacts.Facts;
import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 23.03.16.
 */
public class MetricEnterToSea {

    static Map<String, List<String>> columnBaseAlgorithm = new HashMap<>();
    static List<String> randomSampleName = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        String sampleNumber = "2";
        getMetricsRes(sampleNumber);
    }


    public static void getMetricsRes(String sampleNumber) throws IOException {

        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);

        Map<String, Integer> gentleMap = new HashMap<>();
        Map<String, Integer> uncomfortableMap = new HashMap<>();

        List<String> gentleWords = Arrays.asList("пологий", "плавный", "постепенный", "плоский");
        List<String> uncomfortableWords = Arrays.asList("неудобный", "каменистый", "крутой", "резкий", "крутоват", "глубоко", "становитьсяглубоко");

        for (Hotel h : hotels) {

            String name = h.getName().replaceAll(" ", "");

            if (name.equals("ХилтонХургадаРезорт")) {
                //тут false url
                continue;
            }


            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);
            } catch (IOException e) {
                continue;
            }


//            System.out.println(name);

            for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                for (String keyWord : Arrays.asList("вход", "заход")) {

                    if (entry.getKey().equals(keyWord)) {
                        for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
//                            System.out.println(entry.getKey() + "=" + in.getKey());
                            if (gentleWords.contains(in.getKey())) {
                                gentleMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                            if (uncomfortableWords.contains(in.getKey())) {
                                uncomfortableMap.compute(name, (w, f) -> f == null ? 1 : f + 1);
                            }

                        }

                        randomSampleName.add(name + " вход");
                    }

                }

            }

        }


        System.out.println("вход пологий\nколичество отелей из 100 = "
                + gentleMap.size() + "\nколичество отелей, в которых на самом деле пишут про пологий вход = ?" + "\n" + gentleMap + "\n");
        System.out.println("вход крутой\nколичество отелей из 100 = " +
                uncomfortableMap.size() + "\nколичество отелей, в которых на самом деле пишут про крутой вход= ?" + "\n" + uncomfortableMap + "\n");


    }

    public static Map<String, List<String>> getColumnList(String sampleNumber) throws IOException {
        getMetricsRes(sampleNumber);
        return columnBaseAlgorithm;
    }

    public static List<String> getRandomSampleName(String sampleNumber) throws IOException {
        getMetricsRes(sampleNumber);
        return randomSampleName;
    }


    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/wi-fiFree.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }


        return map;
    }
}
