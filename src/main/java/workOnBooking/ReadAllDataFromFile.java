package workOnBooking;

/**
 * Created by Stepan on 29.03.16.
 */

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ReadAllDataFromFile {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {

        File inputFile = new File("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking.com.merged.reviews.final.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        StringBuilder builderKey;
        StringBuilder builderValue;


        NodeList nList = doc.getElementsByTagName("review");
        for (int i = 0; i < nList.getLength(); i++) {

            builderKey = new StringBuilder();
            builderValue = new StringBuilder();


            Node node = nList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                NodeList nodeList = element.getElementsByTagName("item");
                for (int j = 0; j < nodeList.getLength(); j++) {
                    Node inNode = nodeList.item(j);
                    Element inElement = (Element) inNode;
                    builderKey.append(inElement.
                            getElementsByTagName("fn").
                            item(0).
                            getTextContent()).append(",");

//                    builderKey.append(inElement.
//                            getElementsByTagName("localurl").
//                            item(0).
//                            getTextContent());
                }

                builderValue.append(element.
                        getElementsByTagName("pro").
                        item(0).
                        getTextContent());
                try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reviews/bookingHotels/" + builderKey.toString() + ".out", true))) {
                    out.println(builderValue.toString());
                }

            }
        }
    }

}

