package workOnBooking;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stax.StAXSource;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Stepan on 25.04.16.
 */
public class ReadAllDataUrlReviwesCnt {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, XMLStreamException, TransformerException {

        XMLInputFactory xif = XMLInputFactory.newInstance();
        XMLStreamReader xsr = xif.createXMLStreamReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking.com.merged.reviews.final.xml"));
        xsr.nextTag();

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        Set<Integer> set = new HashSet<>();

        String oldKey = "old";
        String oldUrl = "old";
        int cnt = 0;
        int keyCnt = 0;
        StringBuilder builderKey;
        StringBuilder builderResult = new StringBuilder();
        StringBuilder builderUrl = new StringBuilder();
        String key = "noName";

        while (xsr.nextTag() == XMLStreamConstants.START_ELEMENT) {
            DOMResult result = new DOMResult();
            t.transform(new StAXSource(xsr), result);
            Node domNode = result.getNode();
            NodeList nList = domNode.getChildNodes();

            for (int i = 0; i < nList.getLength(); i++) {

                builderKey = new StringBuilder();
                builderUrl = new StringBuilder();


                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    NodeList nodeList = element.getElementsByTagName("item");
                    for (int j = 0; j < nodeList.getLength(); j++) {
                        Node inNode = nodeList.item(j);
                        Element inElement = (Element) inNode;
                        builderKey.append(inElement.
                                getElementsByTagName("fn").
                                item(0).
                                getTextContent());

                        builderUrl.append(inElement.
                                getElementsByTagName("localurl").
                                item(0).
                                getTextContent());


                        key = builderKey.toString();
                        key = key.replace('/', '@');

//                        if (!oldKey.equals(key)) {
//                            builderResult.append(oldKey).append("\t").append(oldUrl).append("\t").append(cnt).append("\n");
//                            set.add(cnt);
//                            keyCnt++;
//                            cnt = 0;
//
//                        }
//                        cnt++;

                        if (!oldUrl.equals(builderUrl.toString())) {
                        } else {
                            keyCnt++;
                        }

                    }
                }

            }

            oldKey = key;
            oldUrl = builderUrl.toString();


        }

//        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingNameUrlCnt.out"))) {
//            out.println(builderResult.toString());
//        }
//        set.forEach(s -> System.out.print(s + " "));

        System.out.println(keyCnt);

    }
}
