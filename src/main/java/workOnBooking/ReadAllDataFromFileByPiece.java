package workOnBooking;

/**
 * Created by Stepan on 29.03.16.
 */

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stax.StAXSource;
import java.io.*;

public class ReadAllDataFromFileByPiece {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, XMLStreamException, TransformerException {

        try (PrintWriter outFile = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingHotelsName.out", true))) {

            XMLInputFactory xif = XMLInputFactory.newInstance();
            XMLStreamReader xsr = xif.createXMLStreamReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking.com.merged.reviews.final.xml"));
            xsr.nextTag();

            int cc = 0;
            int ccFind = 0;
            int ccNotFind = 0;
            int iter = 0;
            int uniqueUrl = 0;
            String oldUrl = "";

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            while (xsr.nextTag() == XMLStreamConstants.START_ELEMENT) {
                DOMResult result = new DOMResult();
                t.transform(new StAXSource(xsr), result);
                Node domNode = result.getNode();
                NodeList nList = domNode.getChildNodes();

                StringBuilder builderKey;
                StringBuilder builderValue;
                StringBuilder builderUrl;
                for (int i = 0; i < nList.getLength(); i++) {

                    builderKey = new StringBuilder();
                    builderValue = new StringBuilder();
                    builderUrl = new StringBuilder();


                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        String key = "noName";
                        NodeList nodeList = element.getElementsByTagName("item");
                        for (int j = 0; j < nodeList.getLength(); j++) {
                            Node inNode = nodeList.item(j);
                            Element inElement = (Element) inNode;
//                            builderKey.append(inElement.
//                                    getElementsByTagName("fn").
//                                    item(0).
//                                    getTextContent());

                            builderUrl.append(inElement.
                                    getElementsByTagName("localurl").
                                    item(0).
                                    getTextContent());

                            cc++;
                            iter++;

//                            System.out.println(oldUrl);
                            if (builderUrl.toString().equals(oldUrl)) {
                                continue;
                            } else {
                                uniqueUrl++;
                            }
//                            System.out.println(builderUrl.toString());
                            oldUrl = builderUrl.toString();


                            BufferedReader brBase = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/booking_features.tsv"));

                            String line;
                            boolean flag = false;
                            while ((line = brBase.readLine()) != null) {
                                String[] p = line.split("\t");
                                if (builderUrl.toString().equals(p[0])) {
                                    flag = true;
                                    break;
                                }
                            }

                            if (flag) {
//                                System.out.println("find " + builderUrl.toString());
                                ccFind++;
                            } else {
//                                System.out.println("not find " + builderUrl.toString());
                                ccNotFind++;
                            }

                            if (iter > 10_000){
                                System.out.println("number of hotels reviews = " + cc);
                                System.out.println("url find " + ccFind);
                                System.out.println("not find url " + ccNotFind);
                                System.out.println("unique url " + uniqueUrl);
                                return;
                            }

//                            key = builderKey.toString();
//                            key = key.replace('/', '@');

                        }

//                        builderValue.append(element.
//                                getElementsByTagName("pro").
//                                item(0).
//                                getTextContent());

//                        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reviews/bookingHotels/" + key + ".out", true))) {
//                            out.println(builderValue.toString());
//                        }
//
//                        outFile.println(key + "\t" + builderUrl.toString());

                    }
                }
            }

            System.out.println("number of hotels = " + cc);
            System.out.println("url find " + ccFind);
            System.out.println("not find url " + ccNotFind);
            System.out.println("unique url" + uniqueUrl);

        }

    }

}

