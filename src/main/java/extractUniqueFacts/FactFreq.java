package extractUniqueFacts;

/**
 * Created by Stepan on 16.03.16.
 */
public class FactFreq implements Comparable<FactFreq> {
    String fact;
    double tf_idf;

    public FactFreq(String fact, double tf_idf) {
        this.fact = fact;
        this.tf_idf = tf_idf;
    }

    @Override
    public int compareTo(FactFreq fact) {
        if (this.tf_idf < fact.tf_idf) {
            return 1;
        } else {
            if (this.tf_idf > fact.tf_idf) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    @Override
    public String toString() {
        return "factName = " + fact + " tf_idf = " + tf_idf;
    }
}
