package extractUniqueFacts;

import exstractFacts.Facts;
import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.IOException;
import java.util.*;

/**
 * Created by Stepan on 16.03.16.
 */
public class UniqueFactsInEachHotel {
    public static void main(String[] args) throws IOException {
        Map<Hotel, List<FactFreq>> mapHotelToFactTf_Idf = getAllTfIdf();

        for (Map.Entry<Hotel, List<FactFreq>> entryMap : mapHotelToFactTf_Idf.entrySet()) {
            System.out.println(entryMap.getKey().getName());
            List<FactFreq> list = entryMap.getValue();
            Collections.sort(list);

            for (int i = 0; i < 10; i++) {
                System.out.println(list.get(i));
            }
        }
    }


    private static Map<Hotel, Map<String, Integer>> getMapHotelToMapFactFrequency() throws IOException {
        String sampleNumber = "2";

        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);

        Map<Hotel, Map<String, Integer>> allTF = new HashMap<>();

        for (Hotel h : hotels) {

            Map<String, Integer> pairMap = new HashMap<>();

            String name = h.getName().replaceAll(" ", "");
            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);
            } catch (IOException e) {
                continue;
            }

            for (Map.Entry<String, Map<String, Integer>> entryMap : facts.entrySet()) {
                for (Map.Entry<String, Integer> in : entryMap.getValue().entrySet()) {
                    String fact = entryMap.getKey() + "=" + in.getKey();
                    Integer cnt = in.getValue();
                    pairMap.compute(fact, (w, f) -> f == null ? cnt : f + cnt);
                }
            }

            allTF.put(h, pairMap);
        }

        return allTF;

    }

    public static Map<Hotel, List<FactFreq>> getAllTfIdf() throws IOException {
        //числитель - количество документов
        int docCount = 100;
        // знаменатель — количество документов, в которых встречается  t_{i}  (когда  n_{i} \neq 0).
        Map<Hotel, Map<String, Integer>> mapHotelToMapFactFreq = getMapHotelToMapFactFrequency();

        Map<String, Integer> mapFactDocCount = getMapFactDocCount();


        Map<Hotel, List<FactFreq>> mapHotelToFactTf_Idf = new HashMap<>();

        //для каждого документа(отеля)
        for (Map.Entry<Hotel, Map<String, Integer>> entryMap : mapHotelToMapFactFreq.entrySet()) {
            for (Map.Entry<String, Integer> in : entryMap.getValue().entrySet()) {
                Integer tf = in.getValue();
                Integer factDocCount = mapFactDocCount.get(in.getKey());

                Double idf = 0d;
                try {
                    idf = Math.log(docCount / factDocCount);
                } catch (NullPointerException e) {
                    continue;
                }
                Double tf_idf = tf * idf;

                if (mapHotelToFactTf_Idf.containsKey(entryMap.getKey())) {
                    List<FactFreq> list = mapHotelToFactTf_Idf.get(entryMap.getKey());
                    list.add(new FactFreq(in.getKey(), tf_idf));
                    mapHotelToFactTf_Idf.put(entryMap.getKey(), list);
                } else {
                    List<FactFreq> list = new ArrayList<>();
                    list.add(new FactFreq(in.getKey(), tf_idf));
                    mapHotelToFactTf_Idf.put(entryMap.getKey(), list);
                }

            }
        }

        return mapHotelToFactTf_Idf;
    }

    public static Map<String, Integer> getMapFactDocCount() throws IOException {
        String sampleNumber = "2";

        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);

        Map<String, Integer> Idf = new HashMap<>();


        for (Hotel h : hotels) {

            String name = h.getName().replaceAll(" ", "");
            Map<String, Map<String, Integer>> facts;
            try {
                facts = Facts.getFactsFromTomitaOut(sampleNumber, "Tomita" + name);
            } catch (IOException e) {
                continue;
            }

            for (Map.Entry<String, Map<String, Integer>> entryMap : facts.entrySet()) {
                for (Map.Entry<String, Integer> in : entryMap.getValue().entrySet()) {
                    String fact = entryMap.getKey() + "=" + in.getKey();
                    Idf.compute(fact, (w, f) -> f == null ? 1 : f + 1);
                }
            }

        }

        return Idf;
    }


}
