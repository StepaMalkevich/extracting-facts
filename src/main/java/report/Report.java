package report;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.simple.parser.ParseException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Stepan on 25.11.15.
 */
public class Report {
    public static void main(String[] args) throws IOException, ParseException {
        String sampleNumber = "2";
        writeHotelsInfoInXLSFile(sampleNumber, CollecterOfHotelInfo.startCollect(sampleNumber));
    }

    private static void writeHotelsInfoInXLSFile(String sampleNumber, List<HotelInfo> hotelInfoList) throws IOException {
        String filename = "/Users/Stepan/malkevich.stepan/extracting-facts/reports/wi-fiFree/" + sampleNumber + "_top_100WiFiReport.xls";
        HSSFWorkbook workbook = new HSSFWorkbook();
        System.out.println("start creating");
        HSSFSheet sheet = workbook.createSheet("Report");
        HSSFRow rowhead = sheet.createRow(0);
        rowhead.createCell(0).setCellValue("Id");
        rowhead.createCell(1).setCellValue("Name");
        rowhead.createCell(2).setCellValue("Url");
        rowhead.createCell(3).setCellValue("Base");
        rowhead.createCell(4).setCellValue("Algorithm");
        rowhead.createCell(5).setCellValue("(Free, Paid)");
        rowhead.createCell(6).setCellValue("HotelWebsite");

        int i = 1;

        for (HotelInfo info : hotelInfoList) {
            HSSFRow row = sheet.createRow(i);
            row.createCell(0).setCellValue(info.getId());
            row.createCell(1).setCellValue(info.getName());
            row.createCell(2).setCellValue(info.getUrl());
            row.createCell(3).setCellValue(info.getValueBase());
            row.createCell(4).setCellValue(info.getValueAlgorithm());
            row.createCell(5).setCellValue(info.getValueIdeal());
            row.createCell(6).setCellValue(info.getWebsite());

            i++;
        }


        FileOutputStream out = new FileOutputStream(filename);
        workbook.write(out);
        out.close();
        System.out.println("create successful");

    }
}
