package report;

import metric.MetricForWiFi;
import org.json.simple.parser.ParseException;
import workOnTripadvisor.ActionOnTripadvisor;
import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Stepan on 01.12.15.
 */
public class CollecterOfHotelInfo {

    public static List<HotelInfo> startCollect(String sampleNumber) throws IOException, ParseException {
        List<HotelInfo> hotelInfo = new ArrayList<>();

        //в map хранится name,base,algorithm
        Map<String, List<String>> columnMap = MetricForWiFi.getColumnList(sampleNumber);
        System.out.println("map size = " + columnMap.size());

        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleNumber + "_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);
        for (Hotel h : hotels) {
            try {
                String name = h.getName().replaceAll(" ", "");
                //оценка алгоритма и базы
                //случай с noReviews обработан
                String freeFalse;
                String website;

                String base = columnMap.get(name).get(0);
                String conclusionFromReview = columnMap.get(name).get(1);
                String conclusion = conclusionFromReview;
                if (conclusionFromReview.contains(base)) {
                    conclusion = base;
                }

                if (base.equals(conclusion)) {
                    freeFalse = base;
                    website = base;
                } else {
                    freeFalse = "(" + columnMap.get(name).get(2) + ", " + columnMap.get(name).get(3) + ")";
                    website = "разметить руками";
                }


                hotelInfo.add(
                        new HotelInfo(
                                h.getId(),
                                name,
                                getUrl(h),
                                base,
                                conclusion,
                                freeFalse,
                                website
                        ));
            } catch (NullPointerException e) {
                System.out.println(h.getName() + " is null!!!");
            }
        }

        return hotelInfo;
    }

    private static String getUrl(Hotel h) throws IOException, ParseException {
        String name = h.getName();
        String name_20 = name.replaceAll(" ", "%20");

        String idInTripadviser = ActionOnTripadvisor.getHotelIdByHTTP("http://www.tripadvisor.ru/TypeAheadJson?geoPages=true&details=true&types=hotel&query=" + name_20 + "&action=API&uiOrigin=MASTHEAD&source=MASTHEAD");
        String url = "http://www.tripadvisor.ru" + idInTripadviser + "#REVIEWS";
        return url;
    }
}
