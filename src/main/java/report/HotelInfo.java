package report;

/**
 * Created by Stepan on 01.12.15.
 */
public class HotelInfo {
    int id;
    String name;
    String url;
    String valueBase;
    String valueAlgorithm;
    String valueIdeal;
    String website;

    public HotelInfo(int id, String name, String url, String valueBase, String valueAlgorithm, String valueIdeal, String website) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.valueBase = valueBase;
        this.valueAlgorithm = valueAlgorithm;
        this.valueIdeal = valueIdeal;
        this.website = website;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getValueBase() {
        return valueBase;
    }

    public String getValueAlgorithm() {
        return valueAlgorithm;
    }

    public String getValueIdeal() {
        return valueIdeal;
    }

    public String getWebsite() {
        return website;
    }
}
