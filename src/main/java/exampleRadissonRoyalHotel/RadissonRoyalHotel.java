package exampleRadissonRoyalHotel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Stepan on 27.10.15.
 */
public class RadissonRoyalHotel {
    public static void main(String[] args) throws IOException {

        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/Desktop/RadissonReviews.out"))) {
            Document doc = Jsoup.connect("http://www.tripadvisor.ru/Hotel_Review-g298507-d300292-Reviews-Radisson_Royal_Hotel_St_Petersburg-St_Petersburg_Northwestern_District.html#REVIEWS").get();

            Element cnt = doc.getElementsByClass("tabs_pers_counts").first();
            int count = Integer.valueOf(getNumberFromString(cnt.text()));

            extract10ReviewByHotelUrlAndPageNumber(out, "http://www.tripadvisor.ru/Hotel_Review-g298507-d300292-Reviews-Radisson_Royal_Hotel_St_Petersburg-St_Petersburg_Northwestern_District.html#REVIEWS");

            int to = count / 10;
            for (int i = 1; i <= to; i++) {
                String url = "http://www.tripadvisor.ru/Hotel_Review-g298507-d300292-Reviews-or" + i + "0-Radisson_Royal_Hotel_St_Petersburg-St_Petersburg_Northwestern_District.html#REVIEWS";
                extract10ReviewByHotelUrlAndPageNumber(out, url);
            }
        }
    }

    private static void extract10ReviewByHotelUrlAndPageNumber(PrintWriter out, String url) throws IOException {
        Document doc;
        doc = Jsoup.connect(url).get();
        Elements partial = doc.getElementsByClass("partial_entry");
        if (partial.hasText()) {

            Elements contents = doc.getElementsByClass("helpful");
            for (Element content : contents) {
                Elements elementsInHelpful = content.getAllElements();
                for (Element element : elementsInHelpful) {
                    Pattern p = Pattern.compile("helpfulq.*");
                    Matcher m = p.matcher(element.id());
                    if (m.matches()) {
                        String reviewId = getNumberFromString(element.id());
                        doc = Jsoup.connect("http://www.tripadvisor.ru/ExpandedUserReviews-g298507-d300292?target=" + reviewId + "&reviews=" + reviewId + "&servlet=Hotel_Review&expand=1").get();
                        Elements elements = doc.getElementsByClass("entry");
                        out.println("***" + elements.first().text());
                    }
                }
            }
        } else {

            Elements contents = doc.getElementsByAttribute("class");
            for (Element content : contents) {
                Pattern p = Pattern.compile("review_.*");
                Matcher m = p.matcher(content.id());
                if (m.matches()) {
                    String reviewId = getNumberFromString(content.id());
                    doc = Jsoup.connect("http://www.tripadvisor.ru/ExpandedUserReviews-g298507-d300292?target=" + reviewId + "&reviews=" + reviewId + "&servlet=Hotel_Review&expand=1").get();
                    Elements elements = doc.getElementsByClass("entry");
                    out.println("***" + elements.first().text());
                }
            }
        }
    }

    private static String getNumberFromString(String id) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < id.length(); i++) {
            if (Character.isDigit(id.charAt(i))) {
                sb.append(id.charAt(i));
            }
        }
        return sb.toString();
    }
}
