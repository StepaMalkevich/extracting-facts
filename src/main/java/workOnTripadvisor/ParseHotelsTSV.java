package workOnTripadvisor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Stepan on 03.11.15.
 */
public class ParseHotelsTSV {
    public static List<Hotel> parse(String hotelsSample) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(hotelsSample));
        String line;
        List<Hotel> hotels = new ArrayList<>();


        while ((line = br.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, "\t");
            int id = Integer.valueOf(tokenizer.nextToken());
            String name = tokenizer.nextToken();

            String featuresLine = tokenizer.nextToken();
            StringTokenizer tokenizerInFeature = new StringTokenizer(featuresLine, "@@");
            List<String> features = new ArrayList<>();
            while (tokenizerInFeature.hasMoreTokens()) {
                features.add(tokenizerInFeature.nextToken());
            }
            String region = tokenizer.nextToken();

            hotels.add(new Hotel(id, name, features, region));
        }

        return hotels;
    }


}
