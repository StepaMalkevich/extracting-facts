package workOnTripadvisor;

import java.util.List;

/**
 * Created by Stepan on 03.11.15.
 */
public class Hotel {
    private int id;
    private String name;
    private List<String> features;
    private String region;

    public Hotel(int id, String name, List<String> features, String region) {
        this.id = id;
        this.name = name;
        this.features = features;
        this.region = region;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getFeatures() {
        return features;
    }

    public String getRegion() {
        return region;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", features=" + features +
                ", region='" + region + '\'' +
                '}';
    }
}
