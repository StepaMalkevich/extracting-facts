package workOnTripadvisor;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketTimeoutException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Stepan on 03.11.15.
 */
public class ActionOnTripadvisor {

    public static void example() throws IOException {
        Logger log = Logger.getLogger(ActionOnTripadvisor.class.getName());
        FileHandler handler = new FileHandler("test.xml");
        handler.setFormatter(new SimpleFormatter());
        log.addHandler(handler);
        log.info("hello");
        log.log(Level.INFO, "hi");
        try {
            throw new NullPointerException();
        } catch (NullPointerException e) {
            log.log(Level.WARNING, "Exception : ", e);
        }
    }


    public static String getHotelIdByHTTP(String http) throws IOException, ParseException {
        String json = Jsoup.connect(http).timeout(0).ignoreContentType(true).execute().body();

        JSONParser parser = new JSONParser();
        //возвращает весь json
        JSONObject jsonObj = (JSONObject) parser.parse(json);
        //возвращает часть с "result":
        JSONArray jsonInResult = (JSONArray) jsonObj.get("results");
        JSONObject jsonInUrls = (JSONObject) jsonInResult.get(0);

        return jsonInUrls.get("url").toString();
    }

    public static void getAllReviewByHotelIdTripadviser(String fileName, String id) throws IOException {

        int i = 0;

        try (PrintWriter out = new PrintWriter(new FileWriter(fileName))) {

            String globalUrl = "http://www.tripadvisor.ru" + id + "#REVIEWS";
            Document doc = Jsoup.connect(globalUrl).timeout(0).get();

            String[] splitId = id.split("-Reviews-");
            String beforeOr = splitId[0];
            String afterOr = splitId[1];

            String localTripadviserId;//g298507-d300292
            String[] splitHotel_Review = id.split("Hotel_Review-");
            String afterHotel_Review = splitHotel_Review[1];

            String[] splitReviews = afterHotel_Review.split("-Reviews-");
            localTripadviserId = splitReviews[0];


            Element cnt = doc.getElementsByClass("tabs_pers_counts").first();

            extract10ReviewByHotelUrlAndPageNumber(out, globalUrl, localTripadviserId);

            int count;
            try {
                count = Integer.valueOf(getNumberFromString(cnt.text()));
            } catch (NullPointerException e) {
                Application.log.log(Level.WARNING, "Exception : ", e);
                return;
            }

            int to = count / 10;

            System.out.println("reviews: \n pages = " + to + " count = " + count);
            Application.log.log(Level.INFO, "reviews: \n pages = " + to + " count = " + count);

            for (i = 1; i <= to; i++) {
                String url = "http://www.tripadvisor.ru" + beforeOr + "-Reviews-or" + i + "0-" + afterOr + "#REVIEWS";
                extract10ReviewByHotelUrlAndPageNumber(out, url, localTripadviserId);
            }

        } catch (SocketTimeoutException e) {
            Application.log.log(Level.INFO, fileName + ": count of pages before Exception = " + i);
            Application.log.log(Level.WARNING, "java.net.SocketTimeoutException: Read timed out");
        } catch (NullPointerException e) {
            Application.log.log(Level.WARNING, "NullPointerException");
        }

    }

    private static void extract10ReviewByHotelUrlAndPageNumber(PrintWriter out, String url, String localTripadviserId) throws IOException {
        Document doc;
        doc = Jsoup.connect(url).timeout(0).get();
        Elements partial = doc.getElementsByClass("partial_entry");
        //partial_entry означает русский ли отзыв или машинный перевод
        if (partial.hasText()) {
            Elements contents = doc.getElementsByClass("helpful");
            for (Element content : contents) {
                Elements elementsInHelpful = content.getAllElements();
                for (Element element : elementsInHelpful) {
                    Pattern p = Pattern.compile("helpfulq.*");
                    Matcher m = p.matcher(element.id());
                    if (m.matches()) {
                        String reviewId = getNumberFromString(element.id());
                        doc = Jsoup.connect("http://www.tripadvisor.ru/ExpandedUserReviews-" + localTripadviserId + "?target=" + reviewId + "&reviews=" + reviewId + "&servlet=Hotel_Review&expand=1").timeout(0).get();
                        Elements elements = doc.getElementsByClass("entry");
                        out.println(elements.first().text());
                    }
                }
            }
        } else {

            Elements contents = doc.getElementsByAttribute("class");
            for (Element content : contents) {
                Pattern p = Pattern.compile("review_.*");
                Matcher m = p.matcher(content.id());
                if (m.matches()) {
                    String reviewId = getNumberFromString(content.id());
                    doc = Jsoup.connect("http://www.tripadvisor.ru/ExpandedUserReviews-" + localTripadviserId + "?target=" + reviewId + "&reviews=" + reviewId + "&servlet=Hotel_Review&expand=1").timeout(0).get();
                    Elements elements = doc.getElementsByClass("entry");
                    out.println(elements.first().text());
                }
            }
        }
    }

    private static String getNumberFromString(String id) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < id.length(); i++) {
            if (Character.isDigit(id.charAt(i))) {
                sb.append(id.charAt(i));
            }
        }
        return sb.toString();
    }

}
