package workOnTripadvisor;

import org.json.simple.parser.ParseException;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Stepan on 03.11.15.
 */
public class Application {

    static Logger log;

    public static void main(String[] args) throws IOException, ParseException {
        log = Logger.getLogger(Application.class.getName());
        FileHandler handler = new FileHandler("/Users/Stepan/malkevich.stepan/extracting-facts/data/logger/log.txt");
        handler.setFormatter(new SimpleFormatter());
        log.addHandler(handler);
        start();
    }

    public static void start() throws IOException, ParseException {
        String sampleName = "2_top_100";

        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/" + sampleName + "Hotels.tsv";

        //ParseHotelsTSV.parse() возвращает список отелей
        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);

        for (Hotel h : hotels) {
            String name = h.getName();
            String name_20 = name.replaceAll(" ", "%20");
            String fileName = name.replaceAll(" ", "");

            //сохранили в набор файлов список фичей отелей
            try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/hotelFeatures/" + sampleName + "Features/feature" + fileName + ".txt"))) {
                for (String feature : h.getFeatures()) {
                    out.println(feature);
                }
            }


            try {
                //получили локальный id на Tripadviser
                long start = System.currentTimeMillis();
                String idInTripadviser = ActionOnTripadvisor.getHotelIdByHTTP("http://www.tripadvisor.ru/TypeAheadJson?geoPages=true&details=true&types=hotel&query=" + name_20 + "&action=API&uiOrigin=MASTHEAD&source=MASTHEAD");
                System.out.println(idInTripadviser);
                ActionOnTripadvisor.getAllReviewByHotelIdTripadviser(("/Users/Stepan/malkevich.stepan/extracting-facts/reviews/" + sampleName + "SampleTest/" + fileName + ".out"), idInTripadviser);
                long end = System.currentTimeMillis();
                System.out.println("Time for " + fileName + " = " + (end - start) / 60 * 1000 + " min");
            } catch (Exception e) {
                log.log(Level.WARNING, "Exception in class Application!!! : ", e);
            }
        }
    }
}
