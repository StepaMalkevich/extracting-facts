package dataForPlot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Stepan on 25.04.16.
 */
public class BookingDataPlot {
    static int MAX = 10_000;
    static String[] strings = new String[MAX];

    public static void main(String[] args) throws IOException {
        String[] strings = getReviwesCnt();
        for (int i = 0; i < MAX; i++) {
            if (strings[i]!=null){
                System.out.println(i + "\t" + strings[i]);
            }
        }
    }

    public static String[] getReviwesCnt() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/data/bookingNameUrlCnt.out"));

        String line;
        while ((line = br.readLine()) != null) {
            try {
                String[] p = line.split("\t");
                String name = p[0];
                String url = p[1];
                Integer cnt = Integer.valueOf(p[2]);


                String old = strings[cnt];
                String newString = old + ";;;" + name;
                strings[cnt] = newString;
            } catch (Exception e){
                System.out.println(e);
            }
        }
        return strings;
    }
}
