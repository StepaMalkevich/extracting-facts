package dataForPlot.wifi;

import dataForPlot.TripAdvisorDataPlot;
import exstractFacts.Facts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by Stepan on 25.04.16.
 */
public class TripAdvisorWiFiData {
    public static void main(String[] args) throws IOException {
        String[] reviwsCnt = TripAdvisorDataPlot.getReviwesCnt();
        Map<String, String> table = getTable();

        for (int i = 0; i < reviwsCnt.length; i++) {
            if (reviwsCnt[i] != null) {
                String[] hotelsName = reviwsCnt[i].split(";;;");
                //в 0 лежит null
                double y = 0;
                double extractCnt = 0;
                for (int j = 1; j < hotelsName.length; j++) {
                    int free = 0;
                    int paid = 0;


                    Map<String, Map<String, Integer>> facts;
                    try {
                        facts = Facts.getFactsFromTomitaOut("1", "Tomita" + hotelsName[j]);
                    } catch (IOException e) {
                        System.out.println("IOException");
                        continue;
                    }


                    for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                        for (String keyWord : Arrays.asList("wi-fi", "сеть", "вай-фай", "беспроводной", "wi fi", "вай фай", "вай файв", "ва фай", "ви фи", "фай вай",
                                "wireless", "беспроводная сеть", "ви-фи", "wan", "вафушка", "хай-фай", "вафля", "интернет", "беспроводной интернет")) {

                            if (entry.getKey().equals(keyWord)) {
                                for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                                    String s = keyWord + "=" + in.getKey();
                                    if (table.containsKey(s)) {
                                        if (table.get(s).equals("internet_in_hotel=free_wi_fi_in_public_areas")) {
                                            free += in.getValue();
                                        } else {
                                            if (table.get(s).equals("internet_in_hotel=paid_wi_fi_in_public_areas")) {
                                                paid += in.getValue();
                                            }
                                        }
                                    }


                                }
                            }

                        }

                    }

                    if (paid == free && free == 0) {

                    } else {
                        extractCnt++;
                    }

                }
                y = (hotelsName.length - 1) / extractCnt;
                System.out.println(i + "\t" + y);
            }
        }

    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/wi-fiFree.txt"));
        Map<String, String> map = new HashMap<>();
        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }
        return map;
    }


}
