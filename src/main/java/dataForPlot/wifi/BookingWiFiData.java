package dataForPlot.wifi;

import dataForPlot.BookingDataPlot;
import exstractFacts.Facts;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 25.04.16.
 */
public class BookingWiFiData {

    static StringBuilder builder = new StringBuilder();

    public static void main(String[] args) throws IOException {
        String[] reviwsCnt = BookingDataPlot.getReviwesCnt();
        Map<String, String> table = getTable();

        int exp = 0;
        for (int i = 0; i < reviwsCnt.length; i++) {
            if (reviwsCnt[i] != null) {
                String[] hotelsName = reviwsCnt[i].split(";;;");

                int cntWithBeach = 0;
                //в 0 лежит null
                double y = 0;
                double extractCnt = 0;
                for (int j = 1; j < hotelsName.length; j++) {
                    int free = 0;
                    int paid = 0;

                    BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/hotelsOnWi-fi.tsv"));
                    String l;
                    boolean flag = true;
                    while ((l = br.readLine()) != null) {
                        if (l.equals(hotelsName[j])) {
                            flag = false;
                        }
                    }

                    if (flag) {
                        continue;
                    }

                    cntWithBeach++;

                    //почему-то некоторых отелей нет
                    Map<String, Map<String, Integer>> facts;
                    try {
                        facts = Facts.getFactsFromTomitaOutBooking(5, "Tomita" + hotelsName[j]);
                    } catch (NoSuchElementException | IOException e) {
                        exp++;
                        continue;
                    }


                    for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                        for (String keyWord : Arrays.asList("wi-fi", "сеть", "вай-фай", "беспроводной", "wi fi", "вай фай", "вай файв", "ва фай", "ви фи", "фай вай",
                                "wireless", "беспроводная сеть", "ви-фи", "wan", "вафушка", "хай-фай", "вафля", "интернет", "беспроводной интернет")) {

                            if (entry.getKey().equals(keyWord)) {
                                for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                                    String s = keyWord + "=" + in.getKey();
                                    if (table.containsKey(s)) {
                                        if (table.get(s).equals("internet_in_hotel=free_wi_fi_in_public_areas")) {
                                            free += in.getValue();
                                        } else {
                                            if (table.get(s).equals("internet_in_hotel=paid_wi_fi_in_public_areas")) {
                                                paid += in.getValue();
                                            }
                                        }
                                    }


                                }
                            }

                        }

                    }

                    if (paid == free && free == 0) {

                    } else {
                        extractCnt++;
                    }

                }

                builder.append(i).append("\t").append(cntWithBeach).append("\t").append(extractCnt).append("\n");
            }
        }

        System.out.println("exp = " + exp);
        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/wi-fiReportToPlot_v2_onlywithWi-fiInBase", true))) {
            out.println(builder.toString());
        }
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/wi-fiFree.txt"));
        Map<String, String> map = new HashMap<>();
        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }
        return map;
    }

}
