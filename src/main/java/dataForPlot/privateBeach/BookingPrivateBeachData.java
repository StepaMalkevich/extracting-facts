package dataForPlot.privateBeach;

import dataForPlot.BookingDataPlot;
import exstractFacts.Facts;

import java.io.*;
import java.util.*;

/**
 * Created by Stepan on 27.04.16.
 */
public class BookingPrivateBeachData {
    static StringBuilder builder = new StringBuilder();

    public static void main(String[] args) throws IOException {
        String[] reviwsCnt = BookingDataPlot.getReviwesCnt();
        Map<String, String> table = getTable();

        int exp = 0;
        for (int i = 0; i < reviwsCnt.length; i++) {
            if (reviwsCnt[i] != null) {
                String[] hotelsName = reviwsCnt[i].split(";;;");


                int cntWithBeach = 0;
                //в 0 лежит null
                double y = 0;
                double extractCnt = 0;
                for (int j = 1; j < hotelsName.length; j++) {
                    double privateBeach = 0;
                    double publicBeach = 0;


                    BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/hotelsOnbeach.tsv"));
                    String l;
                    boolean flag = true;
                    while ((l = br.readLine()) != null) {
                        if (l.equals(hotelsName[j])) {
                            flag = false;
                        }
                    }

                    if (flag) {
                        continue;
                    }

                    cntWithBeach++;


                    //почему-то некоторых отелей нет
                    Map<String, Map<String, Integer>> facts;
                    try {
                        facts = Facts.getFactsFromTomitaOutBooking(5, "Tomita" + hotelsName[j]);
                    } catch (NoSuchElementException | IOException e) {
                        exp++;
                        continue;
                    }

                    for (Map.Entry<String, Map<String, Integer>> entry : facts.entrySet()) {

                        for (String keyWord : Arrays.asList("пляж")) {

                            if (entry.getKey().equals(keyWord)) {
                                for (Map.Entry<String, Integer> in : entry.getValue().entrySet()) {
                                    String s = keyWord + "=" + in.getKey();
                                    if (table.containsKey(s)) {

                                        if (table.get(s).equals("private_beach=true")) {
                                            privateBeach += in.getValue();
                                        } else {
                                            if (table.get(s).equals("public_beach=true")) {
                                                publicBeach += in.getValue();
                                            }
                                        }
                                    }


                                }
                            }

                        }

                    }

                    if (privateBeach == publicBeach && privateBeach == 0) {

                    } else {
                        extractCnt++;
                    }

                }

                builder.append(i).append("\t").append(cntWithBeach).append("\t").append(extractCnt).append("\n");
            }
        }

        System.out.println("exp = " + exp);
        try (PrintWriter out = new PrintWriter(new FileWriter("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/privateBeachReportToPlot_v2_onlywithBeachInBase", true))) {
            out.println(builder.toString());
        }
    }

    public static Map<String, String> getTable() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/factsTable/privateBeach.txt"));
        Map<String, String> map = new HashMap<>();

        String line;
        StringTokenizer tokenizer;
        while ((line = br.readLine()) != null) {
            tokenizer = new StringTokenizer(line);
            map.put(tokenizer.nextToken(), tokenizer.nextToken());
        }

        return map;
    }
}
