package dataForPlot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stepan on 25.04.16.
 */
public class WorkWithDataToGoInPython {

    static double[] result = new double[50];
    static Map<Integer, Double> map = new HashMap<>();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reports/bookingReport/wi-fiReportToPlot_v2_onlywithWi-fiInBase"));

        String line;
        while ((line = br.readLine()) != null) {
            String[] p = line.split("\t");
            Integer index = Integer.valueOf(p[0]);
            Double hotelsCnt = Double.valueOf(p[1]);
            Double reviwesCnt = Double.valueOf(p[2]);
            result[index] += reviwesCnt;

            map.put(index, hotelsCnt);
        }

        for (int i = 0; i < 50; i++) {
            if (result[i] != 0) {
                Double hotelsCnt = map.get(i);
                Double findWiFiCnt = result[i];
//                System.out.println(i + "\t" + (findWiFiCnt/ hotelsCnt));
                System.out.print((findWiFiCnt / (4 * hotelsCnt)) + ",");
            }
        }

    }
}
