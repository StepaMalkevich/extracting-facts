package dataForPlot;

import workOnTripadvisor.Hotel;
import workOnTripadvisor.ParseHotelsTSV;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by Stepan on 25.04.16.
 */
public class TripAdvisorDataPlot {
    static int MAX = 10_000;
    static String[] strings = new String[MAX];

    public static void main(String[] args) throws IOException {
        getReviwesCnt();
    }

    public static String[] getReviwesCnt() throws IOException {
        String hotelsSampleFile = "/Users/Stepan/malkevich.stepan/extracting-facts/data/1_top_100Hotels.tsv";

        List<Hotel> hotels = ParseHotelsTSV.parse(hotelsSampleFile);

        for (Hotel h : hotels) {
            String name = h.getName().replaceAll(" ", "");
            BufferedReader br = new BufferedReader(new FileReader("/Users/Stepan/malkevich.stepan/extracting-facts/reviews/1_top_100Sample/" + name + ".out"));

            int cnt = 0;
            while (br.readLine() != null) {
                cnt++;
            }

            String old = strings[cnt];
            String newString = old + ";;;" + name;
            strings[cnt] = newString;


        }

        for (int i = 0; i < MAX; i++) {
            if (strings[i]!=null){
                System.out.println(i + "\t" + strings[i]);
            }
        }

        return strings;
    }

}
